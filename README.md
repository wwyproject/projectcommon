# ProjectCommon

[![CI Status](https://img.shields.io/travis/wang.wenyuan/ProjectCommon.svg?style=flat)](https://travis-ci.org/wang.wenyuan/ProjectCommon)
[![Version](https://img.shields.io/cocoapods/v/ProjectCommon.svg?style=flat)](https://cocoapods.org/pods/ProjectCommon)
[![License](https://img.shields.io/cocoapods/l/ProjectCommon.svg?style=flat)](https://cocoapods.org/pods/ProjectCommon)
[![Platform](https://img.shields.io/cocoapods/p/ProjectCommon.svg?style=flat)](https://cocoapods.org/pods/ProjectCommon)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ProjectCommon is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ProjectCommon'
```

## Author

wang.wenyuan, wang.wenyuan@gloritysolutions.com

## License

ProjectCommon is available under the MIT license. See the LICENSE file for more info.
