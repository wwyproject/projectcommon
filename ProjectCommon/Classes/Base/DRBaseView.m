//
//  DRBaseView.m
//  ProjectCommon
//
//  Created by DevWang on 2022/7/19.
//

#import "DRBaseView.h"

@interface DRBaseView ()
{
    NSInteger _count;
}

@end

@implementation DRBaseView

- (void)didAddSubview:(UIView *)subview {
    [super didAddSubview:subview];
    _count++;
    NSLog(@"%@ ==> %ld",NSStringFromSelector(_cmd),(long)_count);
}

- (void)willRemoveSubview:(UIView *)subview {
    [super willRemoveSubview:subview];
    _count++;
    NSLog(@"%@ ==> %ld",NSStringFromSelector(_cmd),(long)_count);
}

- (void)willMoveToSuperview:(nullable UIView *)newSuperview {
    [super willMoveToSuperview:newSuperview];
    _count++;
    NSLog(@"%@ ==> %ld",NSStringFromSelector(_cmd),(long)_count);
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    _count++;
    NSLog(@"%@ ==> %ld",NSStringFromSelector(_cmd),(long)_count);
}

- (void)didMoveToSuperview {
    [super didMoveToSuperview];
    _count++;
    NSLog(@"%@ ==> %ld",NSStringFromSelector(_cmd),(long)_count);
}

- (void)willMoveToWindow:(nullable UIWindow *)newWindow {
    [super willMoveToWindow:newWindow];
    _count++;
    NSLog(@"%@ ==> %ld",NSStringFromSelector(_cmd),(long)_count);
}

- (void)didMoveToWindow {
    [super didMoveToWindow];
    _count++;
    NSLog(@"%@ ==> %ld",NSStringFromSelector(_cmd),(long)_count);
}

- (void)removeFromSuperview {
    [super removeFromSuperview];
    _count++;
    NSLog(@"%@ ==> %ld",NSStringFromSelector(_cmd),(long)_count);
    
}

- (void)dealloc {
    _count++;
    NSLog(@"%@ ==> %ld",NSStringFromSelector(_cmd),(long)_count);
}

- (instancetype)init {
    if (self = [super init]) {
        _count++;
        NSLog(@"%@ ==> %ld",NSStringFromSelector(_cmd),(long)_count);
    }
    return self;
}

@end
