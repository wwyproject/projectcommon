//
//  DRCommonHeader.h
//  Pods
//
//  Created by DevWang on 2022/7/19.
//

#ifndef DRCommonHeader_h
#define DRCommonHeader_h

// Base
#import "DRBaseView.h"

// Category
#import "UIColor+DRColor.h"
#import "UIView+Extension.h"
#import "NSString+DRExtension.h"
#import "NSObject+DRSwizzle.h"
#import "UILabel+DRUtils.h"
#import "UIView+Category.h"
#import "UIView+DRGesture.h"
#import "UIApplication+DRUtils.h"
#import "NSObject+DRExtraData.h"
#import "UIControl+DRAction.h"

// Macro
#import "DRMacroBlock.h"
#import "DRMacroSystem.h"



/** 常用量  */
#pragma mark 常用量
#define Swidth [UIScreen mainScreen].bounds.size.width
#define Sheight [UIScreen mainScreen].bounds.size.height
#define UIColorFromHex(s)  [UIColor colorWithRed:(((s & 0xFF0000) >> 16))/255.0 green:(((s &0xFF00) >>8))/255.0 blue:((s &0xFF))/255.0 alpha:1.0]

#define UIColorFromHexAlph(s,a)  [UIColor colorWithRed:(((s & 0xFF0000) >> 16))/255.0 green:(((s &0xFF00) >>8))/255.0 blue:((s &0xFF))/255.0 alpha:a]

#define kIs_iphone (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define kIs_iPhoneX Swidth >=375.0f && Sheight >=812.0f&& kIs_iphone

/*状态栏高度*/
#define kStatusBarHeight (CGFloat)(kIs_iPhoneX?(44.0):(20.0))
/*导航栏高度*/
#define kNavBarHeight (44)
/*状态栏和导航栏总高度*/
#define kNavBarAndStatusBarHeight (CGFloat)(kIs_iPhoneX?(88.0):(64.0))
/*TabBar高度*/
#define kTabBarHeight (CGFloat)(kIs_iPhoneX?(49.0 + 34.0):(49.0))
/*顶部安全区域远离高度*/
#define kTopBarSafeHeight (CGFloat)(kIs_iPhoneX?(44.0):(0))
 /*底部安全区域远离高度*/
#define kBottomSafeHeight (CGFloat)(kIs_iPhoneX?(34.0):(0))
/*iPhoneX的状态栏高度差值*/
#define kTopBarDifHeight (CGFloat)(kIs_iPhoneX?(24.0):(0))
/*导航条和Tabbar总高度*/
#define kNavAndTabHeight (kNavBarAndStatusBarHeight + kTabBarHeight)
/** 状态栏和Tabbar高度 */
#define KTabAndStatusHeight (kStatusBarHeight + kTabBarHeight)

#define Sheight_all      (Sheight - kNavBarAndStatusBarHeight)


#define APPDelegate ((AppDelegate*)[[UIApplication sharedApplication] delegate])


#endif /* DRCommonHeader_h */
