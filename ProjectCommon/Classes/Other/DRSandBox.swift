//
//  DRSandBox.swift
//  Pods-ProjectCommon_Example
//
//  Created by DevWang on 2022/7/20.
//

import UIKit


@objc (DRSandBox)
public class DRSandBox: NSObject {
    @objc
    public enum SandPathType: Int {
        /** 定位 Document 目录 用于存放需要备份的重要文件 */
        case DOCUMENT_PATH
        /** 定位 Library 目录 */
        case LIBRARY_PATH
        /** 定位 Caches 目录,Caches 目录位于Library目录下 用于存放缓存文件(会定时清理) */
        case CACHES_PATH
        /** 定位 Preference 目录,Preference 目录位于Library目录下 用户存储用户偏好设置 */
        case PREFERENCE_PATH
        /** 定位 Tmp 目录 用于存放临时文件 */
        case TMP_PATH
        /** 定位 Home 主路径 */
        case HOME_PATH
    }
    
    @objc public class func DOCUMENTPATH() -> String {
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    }
    
    @objc public class func LIBRARYPATH() -> String {
        return NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true)[0]
    }
    
    @objc public class func CACHESPATH() -> String {
        return NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0]
    }
    
    @objc public class func PREFERENCEPATH() -> String {
        return NSSearchPathForDirectoriesInDomains(.preferencePanesDirectory, .userDomainMask, true)[0]
    }
    
    /// 获取保存文件的路径
    /// - Parameters:
    ///   - pathtype: 保存位置
    ///   - folderName: 文件夹名称
    ///   - fileName: 文件名称
    /// - Returns: 保存文件路径
    @objc public class func getSaveFilePath(file pathtype: SandPathType, folderName: String?, fileName: String) -> String {
        var docsdir = ""
        
        /** 1.选择添加文件根路径 */
        switch pathtype {
        case .DOCUMENT_PATH:
            docsdir = DOCUMENTPATH()
        case .LIBRARY_PATH:
            docsdir = LIBRARYPATH()
        case .CACHES_PATH:
            docsdir = CACHESPATH()
        case .PREFERENCE_PATH:
            docsdir = PREFERENCEPATH()
        case .TMP_PATH:
            docsdir = NSTemporaryDirectory()
        case .HOME_PATH:
            docsdir = NSHomeDirectory()
        }
        
        /** 2.拼接目标文件夹 */
        var dataFilePath = docsdir
        if let folderName = folderName {
            dataFilePath  = (docsdir as NSString).appendingPathComponent(folderName)
        }
        
        /** 3.检验路径是否正确 */
        let fileManager = FileManager.default
        if !fileManager.fileExists(atPath: dataFilePath) {
            /** 4.若无目标文件夹则新建目标文件夹 */
            do {
               try fileManager.createDirectory(at: URL(fileURLWithPath: dataFilePath), withIntermediateDirectories: true, attributes: nil)
            }
            catch {
                print(error)
            }
        }
        
        /** 5.拼接创建存储文件路径 */
        let filePath = (dataFilePath as NSString).appending(fileName)
        return filePath
    }
    
    /// 删除路径下的文件
    /// - Parameter path: 路径
    /// - Returns: 是否正确删除
    @objc public class func removeFile(file path: String) -> Bool {
        if path.count <= 0 {
            return true
        }
        let fileMsg = FileManager.default
        if fileMsg.fileExists(atPath: path) {
            do {
               try fileMsg.removeItem(atPath: path)
            }
            catch {
                print(error)
            }
        } else {
            return true
        }
        return true
    }
    
}
