//
//  DRCacheArchive.m
//  ProjectCommon
//
//  Created by DevWang on 2022/7/20.
//

#import "DRCacheArchive.h"

@implementation DRCacheArchive

+ (NSData *)archivedData:(id<NSCoding>)object {
    NSData *data = nil;
    @try {
        NSError * error;
        data = [NSKeyedArchiver archivedDataWithRootObject:object requiringSecureCoding:YES error:&error];
    }
    @catch (NSException *exception) {
        
#if DEBUG
        NSLog(@"archive error");
#endif
    }
    return data;
}

+ (id<NSCoding>)unarchiveObject:(NSData *)data {
    id object = nil;
    @try {
        NSError * unarchiveError;
        object = [NSKeyedUnarchiver unarchivedObjectOfClass:[self class] fromData:data error:&unarchiveError];
    }
    @catch (NSException *exception) {
#if DEBUG
        NSLog(@"unarchive error");
#endif
    }
    return object;
}


@end
