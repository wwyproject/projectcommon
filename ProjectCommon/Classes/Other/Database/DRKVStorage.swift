//
//  DRKVStorage.swift
//  ProjectCommon
//
//  Created by DevWang on 2022/7/20.
//

import UIKit
import SQLite3

class GLKVStorageItem: NSObject {
    var key: String?            ///< key
    var value: Data?            ///< value
    var filename: String?       ///< filename (nil if inline)
    var size: Int32 = 0         ///< value's size in bytes
    var modTime: Int32 = 0      ///< modification unix timestamp
    var accessTime: Int32 = 0   ///< last access unix timestamp
}

/*
 File:
 /path/
      /manifest.sqlite
      /manifest.sqlite-shm
      /manifest.sqlite-wal
      /data/
           /e10adc3949ba59abbe56e057f20f883e
           /e10adc3949ba59abbe56e057f20f883e
      /trash/
            /unused_file_or_folder
 
 SQL:
 create table if not exists manifest (
    key                 text,
    filename            text,
    size                integer,
    inline_data         blob,
    modification_time   integer,
    last_access_time    integer,
    primary key(key)
 );
 create index if not exists last_access_time_idx on manifest(last_access_time);
 */

private let kDBFileName = "manifest.sqlite"
private let kDBShmFileName = "manifest.sqlite-shm"
private let kDBWalFileName = "manifest.sqlite-wal"
private let kDataDirectoryName = "data"
private let kTrashDirectoryName = "trash"
private let kMaxErrorRetryCount = 8
private let kMinRetryTimeInterval: TimeInterval = 2.0
private let kPathLengthMax = PATH_MAX - 64

public class DRKVStorage: NSObject {
    var trashQueue: DispatchQueue
    
    var filePath: String
    var dbPath: String
    var dataPath: String
    var trashPath: String
    var db: OpaquePointer?

    var dbStmtCache: Dictionary? = [String : OpaquePointer]()
    let fileManager: FileManager = FileManager.default
    
    var dbLastOpenErrorTime: TimeInterval = 0.0
    var dbOpenErrorCount: Int = 0
    
    init(path: String) {
        if path.count == 0 || path.count > kPathLengthMax {
            debugPrint("GLKVStorage init error: invalid path: [\(path)].")
        }

        trashQueue = DispatchQueue(label: "com.default.glcache.disk.trash")
        
        filePath = path
        dbPath = URL(fileURLWithPath: path).appendingPathComponent(kDBFileName).path
        dataPath = URL(fileURLWithPath: path).appendingPathComponent(kDataDirectoryName).path
        trashPath = URL(fileURLWithPath: path).appendingPathComponent(kTrashDirectoryName).path
        
        super.init()
        
        guard createDirectory() else { return }

        if !dbOpen() || !dbCreateTable() {
            // db file may broken...
            dbClose()
            reset() // rebuild
            if !dbOpen() || !dbCreateTable() {
                dbClose()
                debugPrint("GLKVStorage init error: fail to open sqlite db.")
                return
            }
        }
        fileEmptyTrashInBackground() // empty the trash if failed at last time
    }

    deinit {
        dbClose()
    }
    
    func createDirectory() -> Bool {
        do {
            try fileManager.createDirectory(atPath: filePath, withIntermediateDirectories: true, attributes: nil)
            try fileManager.createDirectory(atPath: dataPath, withIntermediateDirectories: true, attributes: nil)
            try fileManager.createDirectory(atPath: trashPath, withIntermediateDirectories: true, attributes: nil)
        } catch {
            debugPrint("Failed to create folder \(error.localizedDescription)")
            return false
        }
        return true
    }
    
    
    // MARK: - db
    func dbOpen() -> Bool {
        if (db != nil) {
            return true
        }

        let result = sqlite3_open(dbPath, &db)
        if result == SQLITE_OK {
            dbStmtCache = [String : OpaquePointer]()
            dbLastOpenErrorTime = 0
            dbOpenErrorCount = 0
            return true
        } else {
            db = nil
            dbStmtCache = nil
            dbLastOpenErrorTime = CACurrentMediaTime()
            dbOpenErrorCount += 1
            debugPrint("\(#function) line:\(#line) sqlite open failed (\(result)).")
            return false
        }
    }
    
    @discardableResult
    func dbClose() -> Bool {
        if (db == nil) {
            return true
        }

        var result = 0
        var retry = false
        var stmtFinalized = false

        dbStmtCache = nil

        repeat {
            retry = false
            result = Int(sqlite3_close(db))
            if result == SQLITE_BUSY || result == SQLITE_LOCKED {
                if !stmtFinalized {
                    stmtFinalized = true
                    while let stmt: OpaquePointer = sqlite3_next_stmt(db, nil) {
                        sqlite3_finalize(stmt)
                        retry = true
                    }
                }
            } else if result != SQLITE_OK {
                debugPrint("\(#function) line:\(#line) sqlite close failed (\(result)).")
            }
        } while retry
        db = nil
        return true
    }
    
    func dbCheck() -> Bool {
        if (db == nil) {
            if dbOpenErrorCount < kMaxErrorRetryCount && CACurrentMediaTime() - dbLastOpenErrorTime > kMinRetryTimeInterval {
                return dbOpen() && dbCreateTable()
            } else {
                return false
            }
        }
        return true
    }
    
    func dbCreateTable() -> Bool {
        let sql = "pragma journal_mode = wal; pragma synchronous = normal; create table if not exists manifest (key text, filename text, size integer, inline_data blob, modification_time integer, last_access_time integer, primary key(key)); create index if not exists last_access_time_idx on manifest(last_access_time);"
        return dbExecute(sql)
    }
    
    func dbCheckpoint() {
        if !dbCheck() {
            return
        }
        // Cause a checkpoint to occur, merge `sqlite-wal` file to `sqlite` file.
        sqlite3_wal_checkpoint(db, nil)
    }
    
    @discardableResult
    func dbExecute(_ sql: String) -> Bool {
        if sql.count == 0 {
            return false
        }
        if !dbCheck() {
            return false
        }

        var error: UnsafeMutablePointer<Int8>? = nil
        let result = sqlite3_exec(db, sql.cString(using: .utf8), nil, nil, &error)
        if result != SQLITE_OK {
            if let error = String(validatingUTF8:sqlite3_errmsg(db)) {
                debugPrint("\(#function) line:\(#line) sqlite exec error (\(result)): \(error)")
            }
            return false
        }
        return true
    }
    
    func dbPrepareStmt(_ sql: String) -> OpaquePointer? {
        if !dbCheck() || sql.count == 0 || dbStmtCache == nil {
            return nil
        }
        var stmt: OpaquePointer? = dbStmtCache?[sql]
        if stmt == nil {
            if sqlite3_prepare_v2(db, sql.cString(using: .utf8), -1, &stmt, nil) != SQLITE_OK {
                debugPrint("\(#function) line:\(#line) sqlite stmt prepare error: \(String(describing: String(validatingUTF8: sqlite3_errmsg(db))))")
                return nil
            }
            dbStmtCache?[sql] = stmt
        } else {
            sqlite3_reset(stmt)
        }
        return stmt
    }
    
    func dbSave(forKey key: String, value: Data, fileName: String?) -> Bool {
        let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
        let sql = "insert or replace into manifest (key, filename, size, inline_data, modification_time, last_access_time) values (?1, ?2, ?3, ?4, ?5, ?6);"
        guard let stmt = dbPrepareStmt(sql) else {
            return false
        }

        let timestamp = Int(time(nil))
        sqlite3_bind_text(stmt, 1, key.cString(using: .utf8), -1, SQLITE_TRANSIENT)
        sqlite3_bind_text(stmt, 2, fileName?.cString(using: .utf8), -1, SQLITE_TRANSIENT)
        sqlite3_bind_int(stmt, 3, Int32(value.count))
        if let fileName = fileName, fileName.count > 0 {
            sqlite3_bind_blob(stmt, 4, nil, 0, SQLITE_TRANSIENT)
        } else {
            sqlite3_bind_blob(stmt, 4, [UInt8](value), Int32(value.count), SQLITE_TRANSIENT)
        }
        sqlite3_bind_int(stmt, 5, Int32(timestamp))
        sqlite3_bind_int(stmt, 6, Int32(timestamp))

        let result = sqlite3_step(stmt)
        if result != SQLITE_DONE {
            DRCacheAnalytics.analytics(errorCode: .write)
            debugPrint("\(#function) line:\(#line) sqlite insert error (\(result)): \(String(describing: String(validatingUTF8: sqlite3_errmsg(db))))")
            return false
        }
        return true
    }
    
    @discardableResult
    func dbUpdateAccessTime(forKey key: String) -> Bool {
        let sql = "update manifest set last_access_time = ?1 where key = ?2;"
        guard let stmt = dbPrepareStmt(sql) else {
            return false
        }
        sqlite3_bind_int(stmt, 1, Int32(Int(time(nil))))
        sqlite3_bind_text(stmt, 2, key.cString(using: .utf8), -1, nil)
        let result = sqlite3_step(stmt)
        if result != SQLITE_DONE {
            debugPrint("\(#function) line:\(#line) sqlite update error (\(result)): \(String(describing: String(validatingUTF8: sqlite3_errmsg(db))))")
            return false
        }
        return true
    }
    
    @discardableResult
    func dbDeleteItem(forKey key: String) -> Bool {
        let sql = "delete from manifest where key = ?1;"
        guard let stmt = dbPrepareStmt(sql) else {
            return false
        }
        sqlite3_bind_text(stmt, 1, key.cString(using: .utf8), -1, nil)

        let result = sqlite3_step(stmt)
        if result != SQLITE_DONE {
            DRCacheAnalytics.analytics(errorCode: .remove)
            debugPrint("\(#function) line:\(#line) db delete error (\(result)): \(String(describing: String(validatingUTF8: sqlite3_errmsg(db))))")
            return false
        }
        return true
    }
    
    func dbDeleteItems(earlierThan time: TimeInterval) -> Bool {
        let sql = "delete from manifest where last_access_time < ?1;"
        guard let stmt = dbPrepareStmt(sql) else {
            return false
        }
        sqlite3_bind_int(stmt, 1, Int32(time))
        let result = sqlite3_step(stmt)
        if result != SQLITE_DONE {
            debugPrint("\(#function) line:\(#line) sqlite delete error (\(result)): \(String(describing: String(validatingUTF8: sqlite3_errmsg(db))))")
            return false
        }
        return true
    }
    
    func dbGetItem(forKey key: String) -> GLKVStorageItem? {
        let sql = "select key, filename, size, inline_data, modification_time, last_access_time from manifest where key = ?1;"
        guard let stmt = dbPrepareStmt(sql) else {
            return nil
        }
        
        let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
        sqlite3_bind_text(stmt, 1, key.cString(using: .utf8), -1, SQLITE_TRANSIENT)

        var item: GLKVStorageItem? = nil
        let result = sqlite3_step(stmt)
        if result == SQLITE_ROW {
            item = dbGetItem(from: stmt)
        } else {
            if result != SQLITE_DONE {
                DRCacheAnalytics.analytics(errorCode: .read)
                debugPrint("\(#function) line:\(#line) sqlite query error (\(result)): \(String(describing: String(validatingUTF8: sqlite3_errmsg(db))))")
            }
        }
        return item
    }
    
    func dbGetItems(forCount count: Int) -> [GLKVStorageItem] {
        var items: [GLKVStorageItem] = []
        let sql = "select key, filename, size, inline_data, modification_time, last_access_time from manifest order by last_access_time asc limit ?1;"
        guard let stmt = dbPrepareStmt(sql) else {
            return items
        }
        sqlite3_bind_int(stmt, 1, Int32(count))
        
        repeat {
            let result = sqlite3_step(stmt)
            if result == SQLITE_ROW {
                let item = dbGetItem(from: stmt)
                items.append(item)
            } else if result == SQLITE_DONE {
                break
            } else {
                DRCacheAnalytics.analytics(errorCode: .read)
                debugPrint("\(#function) line:\(#line) sqlite query error (\(result)): \(String(describing: String(validatingUTF8: sqlite3_errmsg(db))))")
                break
            }
        } while(true)
        
        return items
    }
    
    func dbGetItem(from stmt: OpaquePointer?) -> GLKVStorageItem {
        let key = sqlite3_column_text(stmt, 0)
        let filename = sqlite3_column_text(stmt, 1)
        let size = sqlite3_column_int(stmt, 2)
        let inline_data = sqlite3_column_blob(stmt, 3)
        let inline_data_bytes = sqlite3_column_bytes(stmt, 3)
        let modification_time = sqlite3_column_int(stmt, 4)
        let last_access_time = sqlite3_column_int(stmt, 5)

        let item = GLKVStorageItem()
        if let key = key {
            item.key = String(cString: key)
        }
        if let filename = filename {
            item.filename = String(cString: filename)
        }
        item.size = size
        if inline_data_bytes > 0, let inline_data = inline_data {
            item.value = Data(bytes: inline_data, count: Int(inline_data_bytes))
        }
        item.modTime = modification_time
        item.accessTime = last_access_time
        return item
    }
    
    func dbGetFilename(forKey key: String) -> String? {
        let sql = "select filename from manifest where key = ?1;"
        guard let stmt = dbPrepareStmt(sql) else {
            return nil
        }
        
        sqlite3_bind_text(stmt, 1, key.cString(using: .utf8), -1, nil)
        let result = sqlite3_step(stmt)
        if result == SQLITE_ROW {
            guard let filename = sqlite3_column_text(stmt, 0) else {
                return nil
            }
            return String(cString: filename)
        } else {
            if result != SQLITE_DONE {
                debugPrint("\(#function) line:\(#line) sqlite query error (\(result)): \(String(describing: String(validatingUTF8: sqlite3_errmsg(db))))")
            }
        }
        return nil
    }
    
    func dbGetFilenames(earlierThan time: TimeInterval) -> [String] {
        var filenames: [String] = []
        let sql = "select filename from manifest where last_access_time < ?1 and filename is not null;"
        guard let stmt = dbPrepareStmt(sql) else {
            return filenames
        }
        sqlite3_bind_int(stmt, 1, Int32(time))

        repeat {
            let result = sqlite3_step(stmt)
            if result == SQLITE_ROW {
                let filename = String(cString: sqlite3_column_text(stmt, 0))
                filenames.append(filename)
            } else if result == SQLITE_DONE {
                break
            } else {
                debugPrint("\(#function) line:\(#line) sqlite query error (\(result)): \(String(describing: String(validatingUTF8: sqlite3_errmsg(db))))")
                break
            }
        } while(true)
        return filenames
    }
    
    func dbGetItemSizeInfoOrderByTimeAscWithLimit(count: Int) -> [GLKVStorageItem?] {
        var items = [GLKVStorageItem]()
        let sql = "select key, filename, size from manifest order by last_access_time asc limit ?1;"
        guard let stmt = dbPrepareStmt(sql) else {
            return items
        }
        sqlite3_bind_int(stmt, 1, Int32(count))
        
        repeat {
            let result = sqlite3_step(stmt)
            if result == SQLITE_ROW {
                let key = sqlite3_column_text(stmt, 0)
                let filename = sqlite3_column_text(stmt, 1)
                let size = sqlite3_column_int(stmt, 2)
                if let key = key {
                    let item = GLKVStorageItem()
                    item.key = String(cString: key)
                    if let filename = filename {
                        item.filename = String(cString: filename)
                    }
                    item.size = size
                    items.append(item)
                }
            } else if result == SQLITE_DONE {
                break
            } else {
                debugPrint("\(#function) line:\(#line) sqlite query error (\(result)): \(String(describing: String(validatingUTF8: sqlite3_errmsg(db))))")
                break
            }
        } while(true)

        return items
    }
    
    func dbGetItemCount(forKey key: String) -> Int {
        let sql = "select count(key) from manifest where key = ?1;"
        guard let stmt = dbPrepareStmt(sql) else {
            return -1
        }
        sqlite3_bind_text(stmt, 1, key.cString(using: .utf8), -1, nil)
        let result = sqlite3_step(stmt)
        if result != SQLITE_ROW {
            debugPrint("\(#function) line:\(#line) sqlite query error (\(result)): \(String(describing: String(validatingUTF8: sqlite3_errmsg(db))))")
            return -1
        }
        return Int(sqlite3_column_int(stmt, 0))
    }
    
    func dbGetTotalItemSize() -> Int {
        let sql = "select sum(size) from manifest;"
        guard let stmt = dbPrepareStmt(sql) else {
            return -1
        }
        let result = sqlite3_step(stmt)
        if result != SQLITE_ROW {
            debugPrint("\(#function) line:\(#line) sqlite query error (\(result)): \(String(describing: String(validatingUTF8: sqlite3_errmsg(db))))")
            return -1
        }
        return Int(sqlite3_column_int(stmt, 0))
    }
    
    func dbGetTotalItemCount() -> Int {
        let sql = "select count(*) from manifest;"
        guard let stmt = dbPrepareStmt(sql) else {
            return -1
        }
        let result = sqlite3_step(stmt)
        if result != SQLITE_ROW {
            debugPrint("\(#function) line:\(#line) sqlite query error (\(result)): \(String(describing: String(validatingUTF8: sqlite3_errmsg(db))))")
            return -1
        }
        return Int(sqlite3_column_int(stmt, 0))
    }
    
    
    // MARK: - file
    func fileWrite(filename: String?, data: Data?) -> Bool {
        if let filename = filename,
           let data = data {
            let path = URL(fileURLWithPath: dataPath).appendingPathComponent(filename).path
            return (try? data.write(to: URL(fileURLWithPath: path))) != nil
        }
        return false
    }
    
    func fileRead(filename: String?) -> Data? {
        if let filename = filename {
            let path = URL(fileURLWithPath: dataPath).appendingPathComponent(filename).path
            let data = fileManager.contents(atPath: path)
            return data
        }
        return nil
    }
    
    @discardableResult
    func fileDelete(filename: String?) -> Bool {
        if let filename = filename {
            let path = URL(fileURLWithPath: dataPath).appendingPathComponent(filename).path
            return (try? fileManager.removeItem(atPath: path)) != nil
        }
        return false
    }
    
    @discardableResult
    func fileMoveAllToTrash() -> Bool {
        let uuidRef = CFUUIDCreate(nil)
        guard let uuid = CFUUIDCreateString(nil, uuidRef) as String? else {
            return false
        }
        let tmpPath = URL(fileURLWithPath: trashPath).appendingPathComponent(uuid).path
        var suc = false
        do {
            try fileManager.moveItem(atPath: dataPath, toPath: tmpPath)
            suc = true
        } catch {
        }
        if suc {
            do {
               try fileManager.createDirectory(atPath: dataPath, withIntermediateDirectories: true, attributes: nil)
            } catch {
            }
        }
        return suc
    }
    
    func fileEmptyTrashInBackground() {
        let trashPath = self.trashPath
        let queue = trashQueue
        queue.async(execute: {
            let manager = FileManager()
            var directoryContents: [String]? = nil
            do {
                directoryContents = try manager.contentsOfDirectory(atPath: trashPath)
            } catch {
            }
            for path in directoryContents ?? [] {
                let fullPath = URL(fileURLWithPath: trashPath).appendingPathComponent(path).path
                do {
                    try manager.removeItem(atPath: fullPath)
                } catch {
                }
            }
        })
    }
    
    
    // MARK: - private
    /**
     Delete all files and empty in background.
     Make sure the db is closed.
     */
    func reset() {
        do {
            try FileManager.default.removeItem(atPath: URL(fileURLWithPath: filePath).appendingPathComponent(kDBFileName).path)
            try FileManager.default.removeItem(atPath: URL(fileURLWithPath: filePath).appendingPathComponent(kDBShmFileName).path)
            try FileManager.default.removeItem(atPath: URL(fileURLWithPath: filePath).appendingPathComponent(kDBWalFileName).path)
        } catch {
        }
        fileMoveAllToTrash()
        fileEmptyTrashInBackground()
    }
    
    
    // MARK: - public
    @discardableResult
    func saveItem(forKey key: String, value: Data, filename: String?) -> Bool {
        if key.count == 0 || value.count == 0 {
            return false
        }

        if let filename = filename, filename.count > 0 {
            if !fileWrite(filename: filename, data: value) {
                return false
            }
            if !dbSave(forKey: key, value: value, fileName: filename) {
                fileDelete(filename: filename)
                return false
            }
            return true
        } else {
            if let filename = dbGetFilename(forKey: key) {
                fileDelete(filename: filename)
            }
            return dbSave(forKey: key, value: value, fileName: nil)
        }
    }
    
    @discardableResult
    func removeItem(forKey key: String) -> Bool {
        if let filename = dbGetFilename(forKey: key) {
            fileDelete(filename: filename)
        }
        return dbDeleteItem(forKey: key)
    }
    
    @discardableResult
    func removeItems(earlierThan time: TimeInterval) -> Bool {
        if time <= 0 {
            return true
        }
        if Int(time) == Int(Int32.max) {
            return removeAllItems()
        }
        let filenames = dbGetFilenames(earlierThan: time)
        for name in filenames {
            fileDelete(filename: name)
        }
        if dbDeleteItems(earlierThan: time) {
            dbCheckpoint()
            return true
        }
        return false
    }
    
    @discardableResult
    func removeItems(toFitSize maxSize: Int) -> Bool {
        if maxSize == Int(Int32.max) {
            return true
        }
        if maxSize <= 0 {
            return removeAllItems()
        }

        var total = dbGetTotalItemSize()
        if total < 0 {
            return false
        }
        if total <= maxSize {
            return true
        }

        var items: [GLKVStorageItem?]
        var suc = false
        repeat {
            let perCount = 16
            items = dbGetItemSizeInfoOrderByTimeAscWithLimit(count: perCount)
            for item in items {
                if total > maxSize {
                    if let filename = item?.filename {
                        fileDelete(filename: filename)
                    }
                    if let key = item?.key {
                        suc = dbDeleteItem(forKey: key)
                    }
                    if let size = item?.size {
                        total -= Int(size)
                    }
                } else {
                    break
                }
                if !suc {
                    break
                }
            }
        } while total > maxSize && items.count > 0 && suc
        if suc {
            dbCheckpoint()
        }
        return suc
    }
    
    @discardableResult
    func removeItems(toFitCount maxCount: Int) -> Bool {
        if maxCount == Int(Int32.max) {
            return true
        }
        if maxCount <= 0 {
            return removeAllItems()
        }

        var total = dbGetTotalItemCount()
        if total < 0 {
            return false
        }
        if total <= maxCount {
            return true
        }

        var items: [GLKVStorageItem?]
        var suc = false
        repeat {
            let perCount = 16
            items = dbGetItemSizeInfoOrderByTimeAscWithLimit(count: perCount)
            for item in items {
                if total > maxCount {
                    if let filename = item?.filename {
                        fileDelete(filename: filename)
                    }
                    if let key = item?.key {
                        suc = dbDeleteItem(forKey: key)
                    }
                    total -= 1
                } else {
                    break
                }
                if !suc {
                    break
                }
            }
        } while total > maxCount && items.count > 0 && suc
        if suc {
            dbCheckpoint()
        }
        return suc
    }
    
    @discardableResult
    func removeAllItems() -> Bool {
        if !dbClose() {
            return false
        }
        reset()
        if !dbOpen() {
            return false
        }
        if !dbCreateTable() {
            return false
        }
        return true
    }
    
    func getItem(forKey key: String) -> GLKVStorageItem? {
        guard let item = dbGetItem(forKey: key) else {
            return nil
        }
        dbUpdateAccessTime(forKey: key)
        if let filename = item.filename {
            item.value = fileRead(filename: filename)
            if item.value == nil {
                dbDeleteItem(forKey: key)
                return nil
            }
        }
        return item
    }
    
    func getItemsCount() -> Int {
        return dbGetTotalItemCount()
    }

    func getItemsSize() -> Int {
        return dbGetTotalItemSize()
    }
    
    func isItemExists(forKey key: String) -> Bool {
        return dbGetItemCount(forKey: key) > 0
    }
}
