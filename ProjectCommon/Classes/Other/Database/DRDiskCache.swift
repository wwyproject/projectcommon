//
//  DRDiskCache.swift
//  ProjectCommon
//
//  Created by DevWang on 2022/7/20.
//

import UIKit
import CommonCrypto

private let cacheIdentifier: String = "com.default.drcache.disk"

/// Free disk space in bytes.
private func DRDiskSpaceFree() -> Int {
    var attrs: [FileAttributeKey : Any]? = nil
    do {
        attrs = try FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory())
    } catch {
    }
    var space = (attrs?[.systemFreeSize] as? NSNumber)?.intValue ?? 0
    if space < 0 {
        space = -1
    }
    return space
}

fileprivate extension String {
    var md5: String {
        let data = Data(self.utf8)
        let hash = data.withUnsafeBytes { (bytes: UnsafeRawBufferPointer) -> [UInt8] in
            var hash = [UInt8](repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))
            CC_SHA256(bytes.baseAddress, CC_LONG(data.count), &hash)
            return hash
        }
        return hash.map { String(format: "%02x", $0) }.joined()
    }
}

/// weak reference for all instances
private var globalInstances: NSMapTable<NSString, DRDiskCache>? = nil
private var globalInstancesLock: DispatchSemaphore? = nil

let DRDiskCacheInitGlobal: Void = {
    globalInstancesLock = DispatchSemaphore(value: 1)
    globalInstances = NSMapTable(keyOptions: .strongMemory, valueOptions: .weakMemory, capacity: 0)
}()

private func DRDiskCacheGetGlobal(_ path: String) -> DRDiskCache? {
    if path.count == 0 {
        return nil
    }
    DRDiskCacheInitGlobal
    globalInstancesLock?.wait()
    let cache: DRDiskCache? = globalInstances?.object(forKey: path as NSString)
    globalInstancesLock?.signal()
    return cache
}

private func DRDiskCacheSetGlobal(_ cache: DRDiskCache?) {
    guard let cache = cache, let path = cache.path, path.count > 0 else {
        return
    }
    DRDiskCacheInitGlobal
    globalInstancesLock?.wait()
    globalInstances?.setObject(cache, forKey: cache.path as NSString?)
    globalInstancesLock?.signal()
}

public class DRDiskCache: NSObject {
    private var name: String?
    fileprivate var path: String?
    private var inlineThreshold: UInt = 0               ///< The default value is 20480 (20KB).
    private var ageLimit: TimeInterval = 0.0            ///< The maximum expiry time of objects in cache.
    private var freeDiskSpaceLimit: UInt = 0            ///< The minimum free disk space (in bytes) which the cache should kept.
    private var autoTrimInterval: TimeInterval = 0.0    ///< The auto trim check time interval in seconds. Default is 60 (1 minute).
    public var countLimit: UInt = 0                     ///< The maximum number of objects the cache should hold.
    public var costLimit: UInt = 0                      ///< The maximum total cost that the cache can hold before it starts evicting objects.

    private var storage: DRKVStorage?
    private let lock = DispatchSemaphore(value: 1)
    private let queue: DispatchQueue = DispatchQueue(label: cacheIdentifier, attributes: DispatchQueue.Attributes.concurrent)
    
    convenience init(path: String) {
        self.init(path: path, inlineThreshold: 1024 * 20) // 20KB
        trimRecursively()
        DRDiskCacheSetGlobal(self);
        NotificationCenter.default.addObserver(self, selector: #selector(appWillBeTerminated), name: UIApplication.willTerminateNotification, object: nil)
    }

    init(path: String, inlineThreshold threshold: Int) {
        
        if DRDiskCacheGetGlobal(path) != nil {
            return
        }
        
        storage = DRKVStorage.init(path: path)
        self.path = path
        inlineThreshold = UInt(threshold)
        countLimit = UInt.max
        costLimit = UInt.max
        ageLimit = Double.greatestFiniteMagnitude
        freeDiskSpaceLimit = 0
        autoTrimInterval = 60
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIApplication.willTerminateNotification, object: nil)
    }
    
    func trimRecursively() {
        DispatchQueue.global().asyncAfter(deadline: .now() + autoTrimInterval) { [weak self] in
            guard let self = self else { return }
            self.trimInBackground()
            self.trimRecursively()
        }
    }
    
    func trimInBackground() {
        queue.async { [weak self] in
            guard let self = self else { return }
            self.Lock()
            self.trim(toCost: self.costLimit)
            self.trim(toCount: self.countLimit)
            self.trim(toAge: self.ageLimit)
            self.trim(toFreeDiskSpace: self.freeDiskSpaceLimit)
            self.Unlock()
        }
    }
    
    private func trim(toCost costLimit: UInt) {
        if costLimit >= Int(Int32.max) {
            return
        }
        storage?.removeItems(toFitSize: Int(costLimit))
    }
    
    private func trim(toCount countLimit: UInt) {
        if countLimit >= Int(Int32.max) {
            return
        }
        storage?.removeItems(toFitCount: Int(countLimit))
    }
    
    private func trim(toAge ageLimit: TimeInterval) {
        if ageLimit <= 0 {
            storage?.removeAllItems()
            return
        }
        let timestamp = Double(time(nil))
        if timestamp <= ageLimit {
            return
        }
        let age = timestamp - ageLimit
        if age >= Double(Int32.max) {
            return
        }
        storage?.removeItems(earlierThan: age)
    }
    
    private func trim(toFreeDiskSpace targetFreeDiskSpace: UInt) {
        if targetFreeDiskSpace == 0 {
            return
        }
        guard let totalBytes = storage?.getItemsSize(), totalBytes > 0 else {
            return
        }

        let diskFreeBytes = DRDiskSpaceFree()
        if diskFreeBytes < 0 {
            return
        }
        let needTrimBytes = targetFreeDiskSpace - UInt(diskFreeBytes)
        if needTrimBytes <= 0 {
            return
        }

        let costLimit = UInt(totalBytes) - UInt(needTrimBytes)
        trim(toCost: costLimit)
    }
    
    func filename(forKey key: String) -> String {
        return key.md5
    }
    
    @objc func appWillBeTerminated() {
        Lock()
        storage = nil
        Unlock()
    }
    
    private func Lock() {
        lock.wait()
    }
    
    private func Unlock() {
        lock.signal()
    }
    
    // MARK: - public
    func setObject(_ object: NSCoding?, forKey key: String, withBlock block: ((_ key: String, _ completion: Bool) -> Void)?) {
        queue.async { [weak self] in
            guard let self = self else { return }
            let suc = self.setObject(object, forKey: key)
            if let block = block {
                block(key, suc)
            }
        }
    }
    
    @discardableResult
    func setObject(_ object: NSCoding?, forKey key: String) -> Bool {
        guard key.count > 0, let storage = storage else {
            return false
        }

        guard let object = object else {
            removeObject(forKey: key)
            return false
        }
        
        guard let value = DRCacheArchive.archivedData(object) as Data? else {
            return false
        }
        
        var filename: String? = nil
        if value.count > inlineThreshold {
            filename = self.filename(forKey: key)
        }

        Lock()
        let suc = storage.saveItem(forKey: key, value: value, filename: filename)
        Unlock()
        return suc
    }
    
    func object(forKey key: String, withBlock block: ((_ key: String, _ object: NSCoding?) -> Void)?) {
        guard let block = block else {
            return
        }
        queue.async(execute: { [weak self] in
            guard let self = self else { return }
            if let object = self.object(forKey: key) {
                block(key, object)
            } else {
                block(key, nil)
            }
        })
    }
    
    func object(forKey key: String) -> NSCoding? {
        guard let storage = storage else {
            return nil
        }
        
        Lock()
        let item = storage.getItem(forKey: key)
        Unlock()
        guard let value = item?.value else {
            return nil
        }
        
        guard let object = DRCacheArchive.unarchiveObject(value) as NSCoding? else {
            return nil
        }

        return object
    }
    
    func removeObject(forKey key: String, withBlock block: ((_ key: String) -> Void)?) {
        queue.async{ [weak self] in
            guard let self = self else { return }
            self.removeObject(forKey: key)
            if let block = block {
                block(key)
            }
        }
    }
    
    func removeObject(forKey key: String) {
        guard let storage = storage else {
            return
        }
        Lock()
        storage.removeItem(forKey: key)
        Unlock()
    }
    
    func removeAllObjects(withBlock block: (() -> Void)?) {
        queue.async{ [weak self] in
            guard let self = self else { return }
            self.removeAllObjects()
            if let block = block {
                block()
            }
        }
    }
    
    func removeAllObjects() {
        guard let storage = storage else {
            return
        }
        Lock()
        storage.removeAllItems()
        Unlock()
    }
    
    func containsObject(forKey key: String, withBlock block: ((_ key: String, _ contains: Bool) -> Void)?) {
        guard let block = block else {
            return
        }
        queue.async(execute: { [weak self] in
            guard let self = self else { return }
            let contains = self.containsObject(forKey: key)
            block(key, contains)
        })
    }
    
    func containsObject(forKey key: String) -> Bool {
        guard let storage = storage else {
            return false
        }
        Lock()
        let contains = storage.isItemExists(forKey: key)
        Unlock()
        return contains
    }
    
    func totalCount() -> Int {
        Lock()
        let count = storage?.getItemsCount() ?? 0
        Unlock()
        return count
    }

    func totalCost() -> Int {
        Lock()
        let count = storage?.getItemsSize() ?? 0
        Unlock()
        return count
    }
}

extension DRDiskCache {
    /// async multiple get
    func objects(forCount count: Int, withBlock block: ((_ objects: [NSCoding]) -> Void)?) {
        guard let block = block else {
            return
        }
        queue.async(execute: { [weak self] in
            guard let self = self else { return }
            let objects = self.objects(forCount: count)
            block(objects)
        })
    }
    
    /// sync multiple get
    func objects(forCount count: Int) -> [NSCoding] {
        var objects: [NSCoding] = []
        guard let storage = storage, count >= 0 else {
            return objects
        }
        
        Lock()
        let items = storage.dbGetItems(forCount: count)
        Unlock()
        for item in items {
            guard let value = item.value else {
                continue
            }
            
            guard let object = DRCacheArchive.unarchiveObject(value) as NSCoding? else {
                continue
            }

            objects.append(object)
        }
        
        return objects
    }
}
