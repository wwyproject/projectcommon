//
//  DRCacheAnalytics.swift
//  ProjectCommon
//
//  Created by DevWang on 2022/7/20.
//

import UIKit

@objc public enum DRCacheError: Int {
    case write
    case read
    case remove
    case archive
    case unarchive
}


@objc public class DRCacheAnalytics: NSObject {
    
    @objc public class func analytics(errorCode: DRCacheError) {
        
    }
}
