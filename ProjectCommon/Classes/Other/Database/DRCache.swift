//
//  DRCache.swift
//  ProjectCommon
//
//  Created by DevWang on 2022/7/20.
//

import UIKit

private let DRCacheName = "com.default.drcache"

@objc (DRCache)
public class DRCache: NSObject {
    
    public static let cache: DRCache = {
        var cache = DRCache(name: DRCacheName)
        return cache
    }()
    
    @objc private(set) public var path: String?
    private var name: String?
    public var memoryCache: NSCache<NSString, NSCoding>?
    public var diskCache: DRDiskCache?
    
    @objc public convenience init(name: String) {
        let cacheFolder = DRSandBox.DOCUMENTPATH()
        let path = URL(fileURLWithPath: cacheFolder).appendingPathComponent(name).path
        
        debugPrint(path)
        self.init(path: path)
    }
    
    init(path: String) {
        if path.count == 0 {
            return
        }
        let diskCache = DRDiskCache(path: path)
        let name = (path as NSString).lastPathComponent
        let memoryCache = NSCache<NSString, NSCoding>()
        memoryCache.name = name

        self.name = name
        self.path = path
        self.diskCache = diskCache
        self.memoryCache = memoryCache
    }
    
    // MARK: - Class Method - For Default Cache Path
    /// class func sync set
    @objc public class func setObject(_ object: NSCoding?, forKey key: String) {
        cache.setObject(object, forKey: key)
    }
    
    /// class func async set with block
    @objc public class func setObject(_ object: NSCoding?, forKey key: String, withBlock block: ((_ key: String, _ completion: Bool) -> Void)?) {
        cache.setObject(object, forKey: key, withBlock: block)
    }
    
    /// class func sync get
    @objc public class func object(forKey key: String) -> NSCoding? {
        return cache.object(forKey: key)
    }

    /// class func async get with block
    @objc public class func object(forKey key: String, withBlock block: ((_ key: String, _ object: NSCoding?) -> Void)?) {
        cache.object(forKey: key, withBlock: block)
    }
    
    /// class func sync remove
    @objc public class func removeObject(forKey key: String) {
        cache.removeObject(forKey: key)
    }

    /// class func async remove with block
    @objc public class func removeObject(forKey key: String, withBlock block: ((_ key: String) -> Void)?) {
        cache.removeObject(forKey: key, withBlock: block)
    }
    
    /// class func sync remove all
    @objc private class func removeAllObjects() {
        cache.removeAllObjects()
    }

    /// class func async remove all with block
    @objc private class func removeAllObjects(withBlock block: (() -> Void)?) {
        cache.removeAllObjects(withBlock: block)
    }
    
    /// class func contains object for key
    @objc public class func contains(forKey key: String) -> Bool {
        return cache.contains(forKey: key)
    }
    
    // MARK: - Class Method - For Custom Cache Path
    /// sync set
    @objc public func setObject(_ object: NSCoding?, forKey key: String) {
        if let object = object {
            memoryCache?.setObject(object, forKey: key as NSString)
        } else {
            memoryCache?.removeObject(forKey: key as NSString)
        }
        diskCache?.setObject(object, forKey: key)
    }
    
    /// async set
    @objc public func setObject(_ object: NSCoding?, forKey key: String, withBlock block: ((_ key: String, _ completion: Bool) -> Void)?) {
        if let object = object {
            memoryCache?.setObject(object, forKey: key as NSString)
        } else {
            memoryCache?.removeObject(forKey: key as NSString)
        }
        diskCache?.setObject(object, forKey: key, withBlock: block)
    }
    
    /// sync get
    @objc public func object(forKey key: String) -> NSCoding? {
        var object = memoryCache?.object(forKey: key as NSString)

        if object == nil {
            object = diskCache?.object(forKey: key)
            if let object = object {
                memoryCache?.setObject(object, forKey: key as NSString)
            }
        }
        return object
    }
    
    /// async get
    @objc public func object(forKey key: String, withBlock block: ((_ key: String, _ object: NSCoding?) -> Void)?) {
        guard let block = block else {
            return
        }
        if let object = memoryCache?.object(forKey: key as NSString) {
            DispatchQueue.global(qos: .default).async {
                block(key, object)
            }
        } else {
            diskCache?.object(forKey: key, withBlock: { [weak self] (key, object) in
                guard let self = self else { return }
                if let object = object, (self.memoryCache?.object(forKey: key as NSString) == nil) {
                    self.memoryCache?.setObject(object, forKey: key as NSString)
                }
                block(key, object)
            })
        }
    }
    
    /// sync remove
    @objc public func removeObject(forKey key: String) {
        memoryCache?.removeObject(forKey: key as NSString)
        diskCache?.removeObject(forKey: key)
    }
    
    /// async remove
    @objc public func removeObject(forKey key: String, withBlock block: ((_ key: String) -> Void)?) {
        memoryCache?.removeObject(forKey: key as NSString)
        diskCache?.removeObject(forKey: key, withBlock: block)
    }
    
    /// sync remove all
    @objc public func removeAllObjects() {
        memoryCache?.removeAllObjects()
        diskCache?.removeAllObjects()
    }

    /// async remove all
    @objc public func removeAllObjects(withBlock block: (() -> Void)?) {
        memoryCache?.removeAllObjects()
        diskCache?.removeAllObjects(withBlock: block)
    }
    
    @objc public func contains(forKey key: String) -> Bool {
        let contains = (diskCache?.containsObject(forKey: key) ?? false)
        return contains
    }
    
}

// MARK: - Extension Class Method - For Default Cache Path
extension DRCache {
    /// class func support sync/async set
    @objc public class func setObject(_ object: NSCoding?, forKey key: String, sync: Bool) {
        if sync {
            cache.setObject(object, forKey: key)
        } else {
            cache.setObject(object, forKey: key, withBlock: nil)
        }
    }
    
    /// class func support sync/async remove
    @objc public class func removeObject(forKey key: String, sync: Bool) {
        if sync {
            cache.removeObject(forKey: key)
        } else {
            cache.removeObject(forKey: key, withBlock: nil)
        }
    }
    
    /// class func support sync/async remove all
    @objc public class func removeAllObjects(sync: Bool) {
        if sync {
            cache.removeAllObjects()
        } else {
            cache.removeAllObjects(withBlock: nil)
        }
    }
    
    /// There is a built-in uniform conversion, but since it is an asynchronous set,
    /// it is recommended to try to get NSUserDefault if GLCache fails to get the Cache.
    @objc public class func migrateNSUserDefaultsToGLCache() {
        let migrateKey = "userdefaults.migtate.glcache"
        if !DRCache.bool(forKey: migrateKey) {
            (UserDefaults.standard.dictionaryRepresentation() as NSDictionary).enumerateKeysAndObjects { (key, obj, stop) in
                if let key = key as? String, let obj = obj as? NSCoding {
                    cache.setObject(obj, forKey: key, withBlock: nil)
                }
            }
            DRCache.setBool(true, forKey: migrateKey)
        }
    }
    
    /// -setBool:forKey:
    @objc public class func setBool(_ value: Bool, forKey key: String) {
        cache.setObject(value as NSCoding, forKey: key)
    }
    
    /// -setInteger:forKey:
    @objc public class func setInteger(_ value: Int, forKey key: String) {
        cache.setObject(value as NSCoding, forKey: key)
    }
    
    /// -setFloat:forKey:
    @objc public class func setFloat(_ value: Float, forKey key: String) {
        cache.setObject(value as NSCoding, forKey: key)
    }
    
    /// -setDouble:forKey:
    @objc public class func setDouble(_ value: Double, forKey key: String) {
        cache.setObject(value as NSCoding, forKey: key)
    }
    
    /// -setData:forKey:
    @objc public class func setData(_ value: Data?, forKey key: String) {
        cache.setObject(value as NSData?, forKey: key)
    }
    
    /// -setString:forKey:
    @objc public class func setString(_ value: String?, forKey key: String) {
        cache.setObject(value as NSString?, forKey: key)
    }
    
    /// -setDictionary:forKey:
    /// Although Swift class works without inheritance, but in order to use NSCoding you must inherit from NSObject.
    /// Do not put a Model that does not inherit from NSObject directly into the dictionary, or it will crash.
    /// `class SwiftModel: NSObject {}`
    @objc public class func setDictionary(_ value: Dictionary<String, Any>?, forKey key: String) {
        cache.setObject(value as NSDictionary?, forKey: key)
    }

    /// -boolForKey
    @objc public class func bool(forKey key: String) -> Bool {
        return cache.object(forKey: key) as? Bool ?? false
    }
    
    /// -integerForKey:
    @objc public class func integer(forKey key: String) -> Int {
        return cache.object(forKey: key) as? Int ?? 0
    }
    
    /// -floatForKey
    @objc public class func float(forKey key: String) -> Float {
        return cache.object(forKey: key) as? Float ?? 0.0
    }
    
    /// -doubleForKey
    @objc public class func double(forKey key: String) -> Double {
        return cache.object(forKey: key) as? Double ?? 0.0
    }
    
    /// -dataForKey:
    @objc public class func data(forKey key: String) -> Data? {
        return cache.object(forKey: key) as? Data
    }
    
    /// -stringForKey:
    @objc public class func string(forKey key: String) -> String? {
        return cache.object(forKey: key) as? String
    }
    
    /// -dictionaryForKey:
    @objc public class func dictionary(forKey key: String) -> Dictionary<String, Any>? {
        return cache.object(forKey: key) as? Dictionary
    }
}


extension DRCache {
    
    /// class func sync multiple get by count
    /// Make sure it's best not on the main thread.
    @objc public class func objects(forCount count: Int) -> [NSCoding] {
        return cache.objects(forCount: count)
    }
    
    /// class func async multiple get by count with block
    @objc public class func objects(forCount count: Int, withBlock block: ((_ object: [NSCoding]) -> Void)?) {
        cache.objects(forCount: count, withBlock: block)
    }
    

    /// sync multiple get
    @objc public func objects(forCount count: Int) -> [NSCoding] {
        return diskCache?.objects(forCount: count) ?? []
    }
    
    /// async multiple get
    @objc public func objects(forCount count: Int, withBlock block: ((_ objects: [NSCoding]) -> Void)?) {
        guard let block = block else {
            return
        }
        diskCache?.objects(forCount: count, withBlock: { (objects) in
            block(objects)
        })
    }
}
