//
//  DRCacheArchive.h
//  ProjectCommon
//
//  Created by DevWang on 2022/7/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DRCacheArchive : NSObject

+ (NSData *)archivedData:(id<NSCoding>)object;

+ (id<NSCoding>)unarchiveObject:(NSData *)data;

@end

NS_ASSUME_NONNULL_END
