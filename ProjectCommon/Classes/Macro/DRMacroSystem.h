//
//  DRMacroSystem.h
//  Pods
//
//  Created by DevWang on 2022/7/20.
//

#ifndef DRMacroSystem_h
#define DRMacroSystem_h

///-------------------------------
/// @define Screen
///-------------------------------
#ifndef ScreenWidth
#define ScreenWidth         ([UIScreen mainScreen].bounds.size.width)
#endif

#ifndef ScreenHeight
#define ScreenHeight        ([UIScreen mainScreen].bounds.size.height)
#endif

#ifndef ScreenScale
#define ScreenScale         ([UIScreen mainScreen].scale)
#endif

#ifndef ScreenBounds
#define ScreenBounds        ([UIScreen mainScreen].bounds)
#endif


///-------------------------------
/// @define Window
///-------------------------------
#ifndef KeyWindow
#define KeyWindow               ([UIApplication sharedApplication].delegate.window)
#endif

///-------------------------------
/// @define Equipment model
///-------------------------------
#ifndef IsIPad
#define IsIPad                  (UIDevice.currentDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad)
#endif

#ifndef IsIPhone5
#define IsIPhone5               (ScreenWidth == 320.0f)
#endif

#ifndef IsIPhoneWidthLessOrEqualTo5
#define IsIPhoneWidthLessOrEqualTo5           (ScreenWidth <= 320.0f)
#endif

#ifndef IsIPhoneWidthLessOrEqualTo6
#define IsIPhoneWidthLessOrEqualTo6           (ScreenWidth <= 375.0f)
#endif

/*
 @3x     1284px x 2778px      428pt x 926pt （ iPhone 12 Pro Max 【放大模式 @3x {1125, 2436}】）
 @3x     1170px x 2532px      390pt x 844pt （ iPhone 12、iPhone 12 Pro ）
 @3x     1080px x 2340px      360pt x 780pt （ iPhone 12 nimi ）
 @3x     1125px x 2436px      375pt x 812pt （ iPhone 12 nimi 模拟器结果 【放大模式 @3x {960, 2079}】）
 @3x      960px x 2079px      320pt x 693pt （ iPhone 12、iPhone 12 Pro 放大模式 ）
 @3x     1242px x 2688px      414pt x 896pt （ iPhone XS Max、iPhone 11 Pro Max ）
 @3x     1125px x 2436px      375pt x 812pt （ iPhone XS、iPhone 11 Pro ）
 @2x      828px x 1792px      414pt x 896pt （ iPhone XR、iPhone 11 ）
 @3x     1125px x 2436px      375pt x 812pt （ iPhone X ）
 
 @2x      750px x 1624px      375pt x 812pt （ 414 x 896放大模式 ）
 */
#ifndef IsIPhoneX
#define IsIPhoneX \
({static BOOL isIPhoneX = NO;\
static dispatch_once_t onceToken;\
dispatch_once(&onceToken, ^{\
    if (@available(iOS 11.0, *)) {\
        isIPhoneX = [[UIApplication sharedApplication] delegate].window.safeAreaInsets.bottom > 0.0f;\
    }\
});\
(isIPhoneX);})
#endif

#ifndef IPhoneXTopHeight
#define IPhoneXTopHeight \
({static CGFloat height = 0.0f;\
static dispatch_once_t onceToken;\
dispatch_once(&onceToken, ^{\
    if (@available(iOS 11.0, *)) {\
        height = IsIPhoneX ? [[UIApplication sharedApplication] delegate].window.safeAreaInsets.top : 0.0f;\
    }\
});\
(height);})
#endif

#ifndef IPhoneXBottomHeight
#define IPhoneXBottomHeight \
({static CGFloat height = 0.0f;\
static dispatch_once_t onceToken;\
dispatch_once(&onceToken, ^{\
    if (@available(iOS 11.0, *)) {\
        height = [[UIApplication sharedApplication] delegate].window.safeAreaInsets.bottom;\
    }\
});\
(height);})
#endif

#ifndef StatusBarHeight
#define StatusBarHeight         (IsIPhoneX ? IPhoneXTopHeight : 20.0f)
#endif

#ifndef NavigationBarHeight
#define NavigationBarHeight     44.0f
#endif

#ifndef NavigationHeight
#define NavigationHeight        (StatusBarHeight + NavigationBarHeight)
#endif

#ifndef TabBarHeight
#define TabBarHeight            (IsIPhoneX ? IPhoneXBottomHeight + 49.0f : 49.0f)
#endif

///-------------------------------
/// @define Adapter
///-------------------------------
#ifndef DRFrameX
#define DRFrameX(x)             (ceil(x / 375.0f * [UIScreen mainScreen].bounds.size.width))
#endif

#ifndef DRFrameY
#define DRFrameY(y)             (ceil(y / 667.0f * [UIScreen mainScreen].bounds.size.height))
#endif

#ifndef DRFrameXIPhone5
#define DRFrameXIPhone5(x)      MIN(GLFrameX(x), x)
#endif

#ifndef DRFrameYIPhone5
#define DRFrameYIPhone5(y)      MIN(GLFrameY(y), y)
#endif

#ifndef DRFrameYIPhoneX
#define DRFrameYIPhoneX(y)      (((y) / (812.f - (y)) * (ScreenHeight - (y))))
#endif

#ifndef DRFrameXIPhone
#define DRFrameXIPhone(x)       (IsIPad ? x : GLFrameX(x))
#endif

#ifndef DRFrameYIPhone
#define DRFrameYIPhone(y)       (IsIPad ? y : GLFrameY(y))
#endif

#ifndef DRFrameSize1
#define DRFrameSize1(iPad, iPhone)                     (IsIPad ? iPad : iPhone)
#endif

#ifndef DRFrameSize2
#define DRFrameSize2(iPad, iPhone6, iPhone5)           (IsIPad ? iPad : (IsIPhone5 ? iPhone5 : iPhone6))
#endif

#ifndef DRFrameSize3
#define DRFrameSize3(iPad, iPhoneX, iPhone6, iPhone5)  (IsIPad ? iPad : (IsIPhoneX ? iPhoneX : (IsIPhone5 ? iPhone5 : iPhone6)))
#endif

///-------------------------------
/// @define Version
///-------------------------------
#ifndef AppVersion
#define AppVersion \
([NSString stringWithFormat:@"%@", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]])
#endif

#ifndef AppBulidVersion
#define AppBulidVersion \
([[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"])
#endif

#ifndef DRSystemVersion
#define DRSystemVersion \
([UIDevice currentDevice].systemVersion.floatValue)
#endif


///-------------------------------
/// @define DRSharedApplication
///-------------------------------
#ifndef DRSharedApplication
#define DRSharedApplication \
[UIApplication sharedApplication]
#endif


///-------------------------------
/// @define DRKeyPath
///-------------------------------
#ifndef DRKeyPath
#define DRKeyPath(target, path) \
@(((void)(NO && ((void)target.path, NO)), # path))
#endif


///-------------------------------
/// @define Log
///-------------------------------
#ifdef  DEBUG
#define DRLog(...) \
NSLog(__VA_ARGS__)
#else
#define DRLog(...) \
{}
#endif

#ifdef  DEBUG
#define DRLog(format, ...) \
NSLog(@"<%@ :%d :%s>-: " format, [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, __FUNCTION__, ##__VA_ARGS__)
#else
#define DRLog(...) \
{}
#endif

///-------------------------------
/// @define ios 11 scroll处理
///-------------------------------
/// TODO 需要观测此方案是否真实需要
#ifndef adjustsScrollViewInsets
#define adjustsScrollViewInsets(scrollView)\
do {\
_Pragma("clang diagnostic push")\
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"")\
if ([scrollView respondsToSelector:NSSelectorFromString(@"setContentInsetAdjustmentBehavior:")]) {\
NSMethodSignature *signature = [UIScrollView instanceMethodSignatureForSelector:@selector(setContentInsetAdjustmentBehavior:)];\
NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:signature];\
NSInteger argument = 2;\
invocation.target = scrollView;\
invocation.selector = @selector(setContentInsetAdjustmentBehavior:);\
[invocation setArgument:&argument atIndex:2];\
[invocation retainArguments];\
[invocation invoke];\
}\
_Pragma("clang diagnostic pop")\
} while (0)
#endif

///-------------------------------
/// @define C
///-------------------------------
#ifdef __cplusplus
#define DR_EXTERN_C_BEGIN extern "C" {
#define DR_EXTERN_C_END }
#else
#define DR_EXTERN_C_BEGIN
#define DR_EXTERN_C_END
#endif

#endif /* DRMacroSystem_h */
