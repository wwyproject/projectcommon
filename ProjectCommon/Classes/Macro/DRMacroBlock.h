//
//  DRMacroBlock.h
//  Pods
//
//  Created by DevWang on 2022/7/20.
//

#ifndef DRMacroBlock_h
#define DRMacroBlock_h

#pragma mark weak self for block
#ifndef DRWeakifySelf
#define DRWeakifySelf \
__weak __typeof(&*self)dr_weak_object = self
#endif

#ifndef DRStrongifySelf
#define DRStrongifySelf \
__strong __typeof(&*dr_weak_object)self = dr_weak_object
#endif

#ifndef DRWeakifyObject1
#define DRWeakifyObject1(name) \
__weak __typeof(&*name)dr_weak_object1 = name
#endif

#ifndef DRStrongifyObject1
#define DRStrongifyObject1(name) \
__strong __typeof(&*dr_weak_object1)name = dr_weak_object1
#endif

#ifndef DRWeakifyObject2
#define DRWeakifyObject2(name) \
__weak __typeof(&*name)gl_weak_object2 = name
#endif

#ifndef DRStrongifyObject2
#define DRStrongifyObject2(name) \
__strong __typeof(&*dr_weak_object2)name = dr_weak_object2
#endif

#ifndef DRWeakifyObject3
#define DRWeakifyObject3(name) \
__weak __typeof(&*name)dr_weak_object3 = name
#endif

#ifndef DRStrongifyObject3
#define DRStrongifyObject3(name) \
__strong __typeof(&*dr_weak_object3)name = dr_weak_object3
#endif

#ifndef dispatch_main_async_safe
#define dispatch_main_async_safe(block)\
    if (dispatch_queue_get_label(DISPATCH_CURRENT_QUEUE_LABEL) == dispatch_queue_get_label(dispatch_get_main_queue())) {\
        block();\
    } else {\
        dispatch_async(dispatch_get_main_queue(), block);\
    }
#endif

#endif /* DRMacroBlock_h */
