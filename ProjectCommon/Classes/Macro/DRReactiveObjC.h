//
//  DRReactiveObjC.h
//  Pods
//
//  Created by DevWang on 2022/7/20.
//

#ifndef DRReactiveObjC_h
#define DRReactiveObjC_h

#import "DRMacroSystem.h"

/// DRRACReceiveNotification
#ifndef DRRACReceiveNotification
#define DRRACReceiveNotification(name) \
([[[NSNotificationCenter defaultCenter] rac_addObserverForName:name object:nil] takeUntil:[self rac_willDeallocSignal]])
#endif


/// DRRACKVO
#ifndef DRRACKVO
#define DRRACKVO(target, keypath) \
({ \
__weak id rac_weak_target = (target); \
[rac_weak_target rac_valuesAndChangesForKeyPath:DRKeyPath(target, keypath) options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) observer:self]; \
})
#endif


/// GLRACObserve
#ifndef DRRACObserve
#define DRRACObserve(target, keypath) \
RACObserve(target, keypath)
#endif

#endif /* DRReactiveObjC_h */
