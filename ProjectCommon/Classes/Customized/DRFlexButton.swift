//
//  DRFlexButton.swift
//  ProjectCommon
//
//  Created by DevWang on 2022/7/19.
//

import UIKit
import SnapKit

@objc (DRFlexButton)
public class DRFlexButton: UIView {
    
    @objc
    public enum DRFlexButtonAlignment: Int {
    case None, Top, Bottom, Right, Left
    }
    
    var clickActionHandle: (() -> ())?
    
    var type: DRFlexButtonAlignment?
    
    var titleImageSpacing = 0
    
    var imageSize: CGSize = .zero
    
    
    // MARK: - Lazy
    
    private lazy var centerView: UIView = {
        UIView()
    }()
    
    private lazy var titleLab: UILabel = {
        var lab = UILabel()
        lab.numberOfLines = 0
        lab.font = UIFont.regular(16.0)
        lab.textColor = UIColor.dr_colorDynamic(0x000000, 0xffffff)
        return lab
    }()
    
    private lazy var imageV: UIImageView = {
        var iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    private lazy var btn: UIButton = {
        var btn = UIButton()
        btn.addTarget(self, action: #selector(clickAction), for: .touchUpInside)
        return btn
    }()
    
    // MARK: - Init
    @objc
    init(button title: String, image: String, type: DRFlexButtonAlignment) {
        self.type = type
        super.init(frame: .zero)
        configUI(title, image: image)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configUI(_ title: String, image: String) {
        titleLab.text = title
        imageV.image = UIImage(named: image)
        imageSize = imageV.image?.size ?? .zero
        initUI()
        layoutUI()
    }
    
    @objc
    public func configLab(lab font: UIFont, txtColor: UIColor) {
        titleLab.font = font
        titleLab.textColor = txtColor
    }
    
    @objc func configImg(image name: String) {
        imageV.image = UIImage(named: name)
    }
    
    // MARK: - Action
    @objc
    public func clickAction() {
        self.clickActionHandle?()
    }
}

extension DRFlexButton {
    
    private func initUI() {
        addSubview(centerView)
        addSubview(btn)
        centerView.addSubview(titleLab)
        centerView.addSubview(imageV)
    }
    
    private func layoutUI() {
        guard let type = self.type else { return }
        
        centerView.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        
        btn.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        switch type {
        case .None: layoutWithNone()
        case .Top: layoutWithTop()
        case .Left: layoutWithLeft()
        case .Bottom: layoutWithBottom()
        case .Right: layoutWithRight()
        }
    }
    
    private func layoutWithNone() {
        
    }
    
    private func layoutWithTop() {
        imageV.snp.makeConstraints { make in
            make.top.centerX.equalToSuperview()
            make.size.equalTo(imageSize)
        }
        
        titleLab.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.top.equalTo(imageV.snp.bottom).offset(titleImageSpacing)
        }
    }
    
    private func layoutWithLeft() {
        imageV.snp.makeConstraints { make in
            make.leading.centerY.equalToSuperview()
            make.size.equalTo(imageSize)
        }
        
        titleLab.snp.makeConstraints { make in
            make.centerY.trailing.equalToSuperview()
            make.leading.equalTo(imageV.snp.trailing).offset(titleImageSpacing)
        }
    }
    
    private func layoutWithBottom() {
        imageV.snp.makeConstraints { make in
            make.bottom.centerX.equalToSuperview()
            make.size.equalTo(imageSize)
            make.top.equalTo(titleLab.snp.bottom).offset(titleImageSpacing)
        }
        
        titleLab.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
        }
    }
    
    private func layoutWithRight() {
        imageV.snp.makeConstraints { make in
            make.trailing.centerY.equalToSuperview()
            make.size.equalTo(imageSize)
        }
        
        titleLab.snp.makeConstraints { make in
            make.centerY.leading.equalToSuperview()
            make.trailing.equalTo(imageV.snp.leading).offset(-titleImageSpacing)
        }
    }

}
