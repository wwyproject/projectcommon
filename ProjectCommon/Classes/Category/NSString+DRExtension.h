//
//  NSString+DRExtension.h
//  ProjectCommon
//
//  Created by DevWang on 2022/7/19.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (DRExtension)

/// 获取字符串的长度和宽度
/// @param text 字符串
/// @param fontSize 字符串尺寸
+ (CGSize)setTextSize:(NSString *)text FontSize:(UIFont *)fontSize;

/**
 获取字符串的长度和宽度 text:字符串 Font:字符串字号
 */
+ (CGSize)setTextSize:(NSString *) text Font:(float)font;


/// 计算字段的Size
/// @param str 需要计算size的字段
/// @param size 显示范围
/// @param font 字段字号
+ (CGSize)setTextWithString:(NSString *)str BoundingRectWithSize:(CGSize)size systemFontOfSize:(UIFont*)font;

/// 计算字段的Size
/// @param str 需要计算size的字段
/// @param width 显示的宽度 高度为最大高度
/// @param font 字段字号
+ (CGSize)setTextWithString:(NSString *)str BoundingRectWithWidth:(CGFloat)width systemFont:(CGFloat)font;

+ (CGSize)setTextWithString:(NSString *)str BoundingRectWithWidth:(CGFloat)width systemFontsize:(UIFont*)font;

+ (CGSize)setTextWithString:(NSString *)str BoundingRectWithHeight:(CGFloat)height systemFontsize:(UIFont*)font;

/// NSString -> Date
/// @param string 日期文字
/// @param dateformat 转化规则
+ (NSDate *)setNSStringToDate:(NSString *)string  dateFormat:(NSString *)dateformat;


/// Date -> NSString
/// @param date 日期Date
/// @param dateformat 转化规则
+ (NSString *)setDateToNSString:(NSDate *)date DateFormat:(NSString *)dateformat;


/// 查询字符串中知否有目标字符
/// @param  queryString 查询字符串
/// @param string 目标字符
+ (BOOL)getWhetherTheQueryStringExists:(NSString *)queryString TargetString:(NSString *)string;


/// 将字符串 中 目标字符 替换
/// @param string 字符串
/// @param str 需要替换的字符
/// @param content 替换成的字符
+ (NSString *)ReplaceTargetString:(NSString *)string TargetString:(NSString *)str ReplaceContent:(NSString *)content;


/// 数组转json
/// @param arrJson 数组
+ (NSString *)dr_jsonStringCompactFormatForNSArray:(NSArray *)arrJson;


/// 从系统配置中获取颜色值
/// @param color 系统配置颜色
+ (NSString *)stringWithConfigurationColor:(NSString *)color;

@end

NS_ASSUME_NONNULL_END
