//
//  NSMutableParagraphStyle+DRUtils.m
//  Pods-ProjectCommon_Example
//
//  Created by DevWang on 2022/7/20.
//

#import "NSMutableParagraphStyle+DRUtils.h"

@implementation NSMutableParagraphStyle (DRUtils)

+ (instancetype)dr_paragraphStyleWithLineHeight:(CGFloat)lineHeight {
    return [self dr_paragraphStyleWithLineHeight:lineHeight lineBreakMode:NSLineBreakByWordWrapping textAlignment:NSTextAlignmentNatural];
}

+ (instancetype)dr_paragraphStyleWithLineHeight:(CGFloat)lineHeight lineBreakMode:(NSLineBreakMode)lineBreakMode {
    return [self dr_paragraphStyleWithLineHeight:lineHeight lineBreakMode:lineBreakMode textAlignment:NSTextAlignmentNatural];
}

+ (instancetype)dr_paragraphStyleWithLineHeight:(CGFloat)lineHeight lineBreakMode:(NSLineBreakMode)lineBreakMode textAlignment:(NSTextAlignment)textAlignment {
    NSMutableParagraphStyle *paragraphStyle = [[self alloc] init];
    paragraphStyle.minimumLineHeight = lineHeight;
    paragraphStyle.maximumLineHeight = lineHeight;
    paragraphStyle.lineBreakMode = lineBreakMode;
    paragraphStyle.alignment = textAlignment;
    return paragraphStyle;
}


@end
