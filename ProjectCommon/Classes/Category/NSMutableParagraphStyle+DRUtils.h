//
//  NSMutableParagraphStyle+DRUtils.h
//  Pods-ProjectCommon_Example
//
//  Created by DevWang on 2022/7/20.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSMutableParagraphStyle (DRUtils)

/// Shortcut for NSMutableParagraphStyle init.
/// Equivalent to `gl_paragraphStyleWithLineHeight:lineBreakMode:NSLineBreakByWordWrapping textAlignment:NSTextAlignmentNatural`
/// @param  lineHeight      line height
/// @return NSMutableParagraphStyle object
+ (instancetype)dr_paragraphStyleWithLineHeight:(CGFloat)lineHeight;

/// Shortcut for NSMutableParagraphStyle init.
/// Equivalent to `gl_paragraphStyleWithLineHeight:lineBreakMode:textAlignment:NSTextAlignmentNatural`
/// @param  lineHeight      line height
/// @param  lineBreakMode   line mode
/// @return NSMutableParagraphStyle object
+ (instancetype)dr_paragraphStyleWithLineHeight:(CGFloat)lineHeight lineBreakMode:(NSLineBreakMode)lineBreakMode;

/// Shortcut for NSMutableParagraphStyle init.
/// @param  lineHeight      line height
/// @param  lineBreakMode   line mode
/// @param  textAlignment   alignment
/// @return NSMutableParagraphStyle object
+ (instancetype)dr_paragraphStyleWithLineHeight:(CGFloat)lineHeight lineBreakMode:(NSLineBreakMode)lineBreakMode textAlignment:(NSTextAlignment)textAlignment;

@end

NS_ASSUME_NONNULL_END
