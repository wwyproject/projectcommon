//
//  NSObject+DRSwizzle.m
//  Pods-ProjectCommon_Example
//
//  Created by DevWang on 2022/7/20.
//

#import "NSObject+DRSwizzle.h"
#import <objc/runtime.h>

@implementation NSObject (DRSwizzle)

- (void)dr_swizzleInstanceMethod:(SEL)originalSelector swizzledSelector:(SEL)swizzledSelector {
    [[self class] dr_swizzleInstanceMethod:originalSelector swizzledSelector:swizzledSelector];
}

- (void)dr_swizzleClassMethod:(SEL)originalSelector swizzledSelector:(SEL)swizzledSelector {
    [[self class] dr_swizzleClassMethod:originalSelector swizzledSelector:swizzledSelector];
}

+ (void)dr_swizzleInstanceMethod:(SEL)originalSelector swizzledSelector:(SEL)swizzledSelector {
    [self dr_swizzleInstanceMethodWithOriginalClass:[self class]
                                      swizzledClass:[self class]
                                   originalSelector:originalSelector
                                   swizzledSelector:swizzledSelector];
}

+ (void)dr_swizzleClassMethod:(SEL)originalSelector swizzledSelector:(SEL)swizzledSelector {
    [self dr_swizzleClassMethodWithOriginalClass:[self class]
                                   swizzledClass:[self class]
                                originalSelector:originalSelector
                                swizzledSelector:swizzledSelector];
}

+ (void)dr_swizzleInstanceMethodWithOriginalClass:(Class)originalClass
                                    swizzledClass:(Class)swizzledClass
                                 originalSelector:(SEL)originalSelector
                                 swizzledSelector:(SEL)swizzledSelector {
    Method originalMethod = class_getInstanceMethod(originalClass, originalSelector);
    Method swizzledMethod = class_getInstanceMethod(swizzledClass, swizzledSelector);
    
    IMP originalIMP = method_getImplementation(originalMethod);
    IMP swizzledIMP = method_getImplementation(swizzledMethod);
    
    const char *originalType = method_getTypeEncoding(originalMethod);
    const char *swizzledType = method_getTypeEncoding(swizzledMethod);
    
    class_replaceMethod(originalClass, swizzledSelector, originalIMP, originalType);
    class_replaceMethod(originalClass, originalSelector, swizzledIMP, swizzledType);
}

+ (void)dr_swizzleClassMethodWithOriginalClass:(Class)originalClass
                                 swizzledClass:(Class)swizzledClass
                              originalSelector:(SEL)originalSelector
                              swizzledSelector:(SEL)swizzledSelector {
    Method originalMethod = class_getClassMethod(originalClass, originalSelector);
    Method swizzledMethod = class_getClassMethod(swizzledClass, swizzledSelector);
    if (!originalMethod || !swizzledMethod) {
        return;
    }
    
    IMP originalIMP = method_getImplementation(originalMethod);
    IMP swizzledIMP = method_getImplementation(swizzledMethod);
    
    const char *originalType = method_getTypeEncoding(originalMethod);
    const char *swizzledType = method_getTypeEncoding(swizzledMethod);
    
    Class metaClass = objc_getMetaClass(class_getName(originalClass));
    class_replaceMethod(metaClass, swizzledSelector, originalIMP, originalType);
    class_replaceMethod(metaClass, originalSelector, swizzledIMP, swizzledType);
}

@end
