//
//  NSObject+DRViewTime.h
//  ProjectCommon
//
//  Created by DevWang on 2022/7/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (DRViewTime)

@property (nonatomic, assign) NSInteger beginTime;

- (void)beginRecordViewTime;
- (NSInteger)endRecordViewTime;

@end

NS_ASSUME_NONNULL_END
