//
//  UIImage+DRColorImage.h
//  Pods-ProjectCommon_Example
//
//  Created by DevWang on 2022/7/20.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (DRColorImage)

/// Get image by color
/// @param color target image color
/// @return image
+ (nullable UIImage *)dr_imageWithColor:(UIColor *)color NS_SWIFT_NAME(init(color:));

/// Get image by color
/// @param color target image color
/// @param size target image size
/// @return image
+ (nullable UIImage *)dr_imageWithColor:(UIColor *)color
                                   size:(CGSize)size NS_SWIFT_NAME(init(color:size:));

@end

NS_ASSUME_NONNULL_END
