//
//  UILabel+DRUtils.m
//  Pods-ProjectCommon_Example
//
//  Created by DevWang on 2022/7/20.
//

#import "UILabel+DRUtils.h"
#import "NSObject+DRSwizzle.h"
#import <objc/runtime.h>
#import "NSMutableParagraphStyle+DRUtils.h"

const CGFloat DRLabelLineHeightIdentity = -1000;

@implementation UILabel (DRUtils)

+ (void)initialize {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self dr_swizzleInstanceMethod:@selector(setText:) swizzledSelector:@selector(dr_setText:)];
        [self dr_swizzleInstanceMethod:@selector(setAttributedText:) swizzledSelector:@selector(dr_setAttributedText:)];
    });
}

- (void)dr_setText:(NSString *)text {
    if (!text) {
        [self dr_setText:text];
        return;
    }
    if (!self.dr_textAttributes.count && ![self _hasSetGLLineHeight]) {
        [self dr_setText:text];
        return;
    }
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:text ?: @"" attributes:self.dr_textAttributes];
    [self dr_setAttributedText:[self attributedStringWithKernAndLineHeightAdjusted:attributedString]];
}

/// Add the new style contained in the \c attributedString passed by the user on top of the \c dr_textAttributes style.
/// If there are style conflicts in this method, \c attributedText takes precedence.
- (void)dr_setAttributedText:(NSAttributedString *)text {
    if (!text || (!self.dr_textAttributes.count && ![self _hasSetGLLineHeight])) {
        [self dr_setAttributedText:text];
        return;
    }
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text.string ?: @"" attributes:self.dr_textAttributes];
    attributedString = [[self attributedStringWithKernAndLineHeightAdjusted:attributedString] mutableCopy];
    [text enumerateAttributesInRange:NSMakeRange(0, text.length)
                             options:0
                          usingBlock:^(NSDictionary<NSString *, id> *_Nonnull attrs, NSRange range, BOOL *_Nonnull stop) {
                              [attributedString addAttributes:attrs range:range];
                          }];
    [self dr_setAttributedText:attributedString];
}


static char kAssociatedObjectKey_textAttributes;
/// Add the dr_textAttributes style to the existing style.
/// If there is a style conflict in this method, dr_textAttributes will prevail.
- (void)setDr_textAttributes:(NSDictionary<NSAttributedStringKey, id> *)dr_textAttributes {
    NSDictionary *prevTextAttributes = self.dr_textAttributes;
    if ([prevTextAttributes isEqualToDictionary:dr_textAttributes]) {
        return;
    }

    objc_setAssociatedObject(self, &kAssociatedObjectKey_textAttributes, dr_textAttributes, OBJC_ASSOCIATION_COPY_NONATOMIC);

    if (!self.text.length) {
        return;
    }
    NSMutableAttributedString *string = [self.attributedText mutableCopy];
    NSRange fullRange = NSMakeRange(0, string.length);

    /// 1) The current attributedText contains styles that can be set by dr_textAttributes or by passing directly to attributedString.
    /// In this case, the styling effects of the former are filtered out and the styling effects of the latter remain.
    if (prevTextAttributes) {
        /// Find out which attrs in the current attributedText were set with the last dr_textAttributes .
        NSMutableArray *willRemovedAttributes = [NSMutableArray array];
        [string enumerateAttributesInRange:NSMakeRange(0, string.length)
                                   options:0
                                usingBlock:^(NSDictionary<NSAttributedStringKey, id> *_Nonnull attrs, NSRange range, BOOL *_Nonnull stop) {
                                    /// If there is a kern attribute, it is only possible to set it through gl_textAttribtus if range is the first word to the penultimate word.
                                    if (NSEqualRanges(range, NSMakeRange(0, string.length - 1)) && [attrs[NSKernAttributeName] isEqualToNumber:prevTextAttributes[NSKernAttributeName]]) {
                                        [string removeAttribute:NSKernAttributeName range:NSMakeRange(0, string.length - 1)];
                                    }
                                    /// If range is not the entire string, it must not be set via gl_textAttributes.
                                    if (!NSEqualRanges(range, fullRange)) {
                                        return;
                                    }
                                    [attrs enumerateKeysAndObjectsUsingBlock:^(NSAttributedStringKey _Nonnull attr, id _Nonnull value, BOOL *_Nonnull stop) {
                                        if (prevTextAttributes[attr] == value) {
                                            [willRemovedAttributes addObject:attr];
                                        }
                                    }];
                                }];
        [willRemovedAttributes enumerateObjectsUsingBlock:^(id _Nonnull attr, NSUInteger idx, BOOL *_Nonnull stop) {
            [string removeAttribute:attr range:fullRange];
        }];
    }

    /// 2) Add a new style
    if (dr_textAttributes) {
        [string addAttributes:dr_textAttributes range:fullRange];
    }
    /// You cannot call setAttributedText:, otherwise if you encounter a style conflict,
    /// that method will make the NSAttributedString style passed in by the user override the dr_textAttributes style.
    [self dr_setAttributedText:[self attributedStringWithKernAndLineHeightAdjusted:string]];
}

- (NSDictionary *)dr_textAttributes {
    return (NSDictionary *)objc_getAssociatedObject(self, &kAssociatedObjectKey_textAttributes);
}

/// Remove the kern effect of the last word and apply dr_setLineHeight: if necessary.
- (NSAttributedString *)attributedStringWithKernAndLineHeightAdjusted:(NSAttributedString *)string {
    if (!string.length) {
        return string;
    }
    NSMutableAttributedString *attributedString = nil;
    if ([string isKindOfClass:[NSMutableAttributedString class]]) {
        attributedString = (NSMutableAttributedString *)string;
    } else {
        attributedString = [string mutableCopy];
    }

    /// Remove the kern effect of the last word, making the text as a whole visually centered.
    /// This should only be adjusted if kern is set in dr_textAttributes.
    if (self.dr_textAttributes[NSKernAttributeName]) {
        [attributedString removeAttribute:NSKernAttributeName range:NSMakeRange(string.length - 1, 1)];
    }

    /// Determines whether the row height set by dr_setLineHeight: should be applied.
    __block BOOL shouldAdjustLineHeight = [self _hasSetGLLineHeight];
    [attributedString enumerateAttribute:NSParagraphStyleAttributeName
                                 inRange:NSMakeRange(0, attributedString.length)
                                 options:0
                              usingBlock:^(NSParagraphStyle *style, NSRange range, BOOL *_Nonnull stop) {
                                  /// If the user has already set the line height for the entire range of text by passing in NSParagraphStyle,
                                  /// then the line height should not be adjusted again.
                                  if (NSEqualRanges(range, NSMakeRange(0, attributedString.length))) {
                                      if (style && (style.maximumLineHeight || style.minimumLineHeight)) {
                                          shouldAdjustLineHeight = NO;
                                          *stop = YES;
                                      }
                                  }
                              }];
    if (shouldAdjustLineHeight) {
        NSMutableParagraphStyle *paraStyle = [NSMutableParagraphStyle dr_paragraphStyleWithLineHeight:self.dr_lineHeight lineBreakMode:self.lineBreakMode textAlignment:self.textAlignment];
        NSDictionary *attributes = @{
            NSParagraphStyleAttributeName : paraStyle,
            NSBaselineOffsetAttributeName : @((self.dr_lineHeight / 2.0 - self.font.lineHeight / 2.0) / 2.0),
        };
        [attributedString addAttributes:attributes range:NSMakeRange(0, attributedString.length)];
    }

    return attributedString;
}

static char kAssociatedObjectKey_lineHeight;
- (void)setDr_lineHeight:(CGFloat)dr_lineHeight {
    if (dr_lineHeight == DRLabelLineHeightIdentity) {
        objc_setAssociatedObject(self, &kAssociatedObjectKey_lineHeight, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    } else {
        objc_setAssociatedObject(self, &kAssociatedObjectKey_lineHeight, @(dr_lineHeight), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    /// Note: For UILabel, attributedText has a value as long as you setText, so there is no need to distinguish between setText and setAttributedText.
    /// Note: You need to refresh the dr_textAttributes style for text, otherwise the lineHeight will not be set.
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.attributedText.string ?: @"" attributes:self.dr_textAttributes];
    attributedString = [[self attributedStringWithKernAndLineHeightAdjusted:attributedString] mutableCopy];
    [self setAttributedText:attributedString];
}

- (CGFloat)dr_lineHeight {
    if ([self _hasSetGLLineHeight]) {
        return [(NSNumber *)objc_getAssociatedObject(self, &kAssociatedObjectKey_lineHeight) floatValue];
    } else if (self.attributedText.length) {
        __block NSMutableAttributedString *string = [self.attributedText mutableCopy];
        __block CGFloat result = 0;
        [string enumerateAttribute:NSParagraphStyleAttributeName
                           inRange:NSMakeRange(0, string.length)
                           options:0
                        usingBlock:^(NSParagraphStyle *style, NSRange range, BOOL *_Nonnull stop) {
                            /// If the user has already set the line height for the entire range of text by passing in NSParagraphStyle,
                            /// then the line height should not be adjusted again
                            if (NSEqualRanges(range, NSMakeRange(0, string.length))) {
                                if (style && (style.maximumLineHeight || style.minimumLineHeight)) {
                                    result = style.maximumLineHeight;
                                    *stop = YES;
                                }
                            }
                        }];

        return result == 0 ? self.font.lineHeight : result;
    } else if (self.text.length) {
        return self.font.lineHeight;
    }

    return 0;
}

- (BOOL)_hasSetGLLineHeight {
    return !!objc_getAssociatedObject(self, &kAssociatedObjectKey_lineHeight);
}

@end
