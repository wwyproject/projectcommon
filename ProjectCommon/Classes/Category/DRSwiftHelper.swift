//
//  DRSwiftHelper.swift
//  ProjectCommon
//
//  Created by DevWang on 2022/7/19.
//

import UIKit
import Foundation

public let ScreenWidth = UIScreen.main.bounds.size.width
public let ScreenHeight = UIScreen.main.bounds.size.height
public let ScreenScale = UIScreen.main.scale
public let ScreenBounds = UIScreen.main.bounds

public func DR_SCREEN_WIDTH(_ size: CGFloat) -> CGFloat { return size * ScreenWidth / 375.0 }
public func DR_SCREEN_HEIGHT(_ size: CGFloat) -> CGFloat {  return size * ScreenHeight / 667.0 }


// MARK: - UIFont
public func GLFontZoom(_ size: CGFloat) -> CGFloat { (size * max((ScreenWidth < 375.0 ? ScreenWidth : 375.0), 320.0) / 375.0) }

public extension UIFont {
    class func regular(_ size: CGFloat)  -> UIFont { .systemFont(ofSize: GLFontZoom(size), weight: .regular) }
    class func thin(_ size: CGFloat)     -> UIFont { .systemFont(ofSize: GLFontZoom(size), weight: .thin) }
    class func light(_ size: CGFloat)    -> UIFont { .systemFont(ofSize: GLFontZoom(size), weight: .light) }
    class func medium(_ size: CGFloat)   -> UIFont { .systemFont(ofSize: GLFontZoom(size), weight: .medium) }
    class func semibold(_ size: CGFloat) -> UIFont { .systemFont(ofSize: GLFontZoom(size), weight: .semibold) }
    class func bold(_ size: CGFloat)     -> UIFont { .systemFont(ofSize: GLFontZoom(size), weight: .bold) }
    class func heavy(_ size: CGFloat)    -> UIFont { .systemFont(ofSize: GLFontZoom(size), weight: .heavy) }
    class func black(_ size: CGFloat)    -> UIFont { .systemFont(ofSize: GLFontZoom(size), weight: .black) }
}
