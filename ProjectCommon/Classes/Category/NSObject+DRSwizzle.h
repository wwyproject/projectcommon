//
//  NSObject+DRSwizzle.h
//  Pods-ProjectCommon_Example
//
//  Created by DevWang on 2022/7/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (DRSwizzle)

/// Swizzle Instance Method
/// @param originalSelector original Method
/// @param swizzledSelector swizzled Method
- (void)dr_swizzleInstanceMethod:(SEL)originalSelector swizzledSelector:(SEL)swizzledSelector;

/// Swizzle Instance Method
/// @param originalSelector original Method
/// @param swizzledSelector swizzled Method
+ (void)dr_swizzleInstanceMethod:(SEL)originalSelector swizzledSelector:(SEL)swizzledSelector;

/// Swizzle Instance Method
/// @param originalClass original Class
/// @param swizzledClass swizzled Class
/// @param originalSelector original Method
/// @param swizzledSelector swizzled Method
+ (void)dr_swizzleInstanceMethodWithOriginalClass:(Class)originalClass
                                    swizzledClass:(Class)swizzledClass
                                 originalSelector:(SEL)originalSelector
                                 swizzledSelector:(SEL)swizzledSelector;


/// Swizzle Class Method
/// @param originalSelector original Method
/// @param swizzledSelector swizzled Method
- (void)dr_swizzleClassMethod:(SEL)originalSelector swizzledSelector:(SEL)swizzledSelector;

/// Swizzle Class Method
/// @param originalSelector original Method
/// @param swizzledSelector swizzled Method
+ (void)dr_swizzleClassMethod:(SEL)originalSelector swizzledSelector:(SEL)swizzledSelector;

/// Swizzle Class Method
/// @param originalClass original Class
/// @param swizzledClass swizzled Class
/// @param originalSelector original Method
/// @param swizzledSelector swizzled Method
+ (void)dr_swizzleClassMethodWithOriginalClass:(Class)originalClass
                                 swizzledClass:(Class)swizzledClass
                              originalSelector:(SEL)originalSelector
                              swizzledSelector:(SEL)swizzledSelector;

@end

NS_ASSUME_NONNULL_END
