//
//  UIView+Category.h
//  ProjectCommon
//
//  Created by DevWang on 2022/7/20.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_OPTIONS(NSUInteger, DRRectCorner) {
    DRRectCornerTopLeft     = UIRectCornerTopLeft,
    DRRectCornerTopRight    = UIRectCornerTopRight,
    DRRectCornerBottomLeft  = UIRectCornerBottomLeft,
    DRRectCornerBottomRight = UIRectCornerBottomRight,
    DRRectCornerTop         = UIRectCornerTopLeft | UIRectCornerTopRight,
    DRRectCornerBottom      = UIRectCornerBottomLeft | UIRectCornerBottomRight,
    DRRectCornerLeft        = UIRectCornerTopLeft | UIRectCornerBottomLeft,
    DRRectCornerRight       = UIRectCornerTopRight | UIRectCornerBottomRight,
    DRRectCornerAll         = UIRectCornerAllCorners
};

@interface UIView (Category)

/// Set rect corner and radius for view.
/// @param corners GLRectCorner
/// @param radius CGFloat
- (void)dr_roundCorners:(DRRectCorner)corners radius:(CGFloat)radius;


/// Remove all subviews.
- (void)dr_removeAllSubviews;


/// Add Subviews.
/// @param views subview array
- (void)dr_addSubviews:(NSArray<UIView *> *)views;


/// Add subview spread auto layout.
/// @param view subview
/// @return constraints
- (NSArray *)dr_addSubviewSpreadAutoLayout:(UIView *)view;


/// Whether the current view is in the specified view.
/// @param view specified view
/// @return isIn
- (BOOL)dr_isInView:(UIView *)view;


/// Get rect in view.
/// @param view specified view
/// @return rect
- (CGRect)dr_convertRectToView:(UIView *)view;


/// Gets the color of a position in the View.
/// @param point (x,y)
/// @return color
- (UIColor *)dr_colorWithPoint:(CGPoint)point;


/// Create a snapshot image of the complete view hierarchy.
/// Scale Default 0
- (nullable UIImage *)dr_snapshotImage;


/// Create a snapshot image of the complete view hierarchy.
- (nullable UIImage *)dr_snapshotImageWithScale:(CGFloat)scale NS_SWIFT_NAME(gl_snapshotImage(scale:));


/// Create a snapshot image of the complete view hierarchy.
/// @discussion It's faster than "snapshotImage", but may cause screen updates.
/// See -[UIView drawViewHierarchyInRect:afterScreenUpdates:] for more information.
- (nullable UIImage *)dr_snapshotImageAfterScreenUpdates:(BOOL)afterUpdates;

@end

NS_ASSUME_NONNULL_END
