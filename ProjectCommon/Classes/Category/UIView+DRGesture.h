//
//  UIView+DRGesture.h
//  ProjectCommon
//
//  Created by DevWang on 2022/7/20.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class DRGestureElement;

typedef void (^DRGestureActionBlock)(__kindof UIView *view);

@interface UIView (DRGesture)

@property (nonatomic, strong) DRGestureElement *dr_tapGestureElement; /// Stores gesture-related attribute elements


/// Perform an action when the View is clicked
/// @param action block
- (void)dr_performActionOnTap:(DRGestureActionBlock)action;

/// Actions are performed when the View is clicked. If the View has a SubView, no actions are performed when the click occurs on the SubView
/// @param action block
/// @param isBackgroundOnly Whether to respond only to a click on the View, not a click on the SubView
- (void)dr_performActionOnTap:(DRGestureActionBlock)action backgroundOnly:(BOOL)isBackgroundOnly;


/// Perform an action when the View is clicked
/// @param target In response to the target
/// @param selector selector
- (void)dr_performSelectorOnTapWithTarget:(id)target selector:(SEL)selector;

/// Perform an action when the View is clicked
/// @param target In response to the target
/// @param selector selector
/// @param isBackgroundOnly Whether to respond only to a click on the View, not a click on the SubView
- (void)dr_performSelectorOnTapWithTarget:(id)target selector:(SEL)selector backgroundOnly:(BOOL)isBackgroundOnly;

@end

@interface DRGestureElement : NSObject <UIGestureRecognizerDelegate>

@property (nonatomic, weak) UIView *target; /// The view of the binding
@property (nonatomic, strong, readonly) UIGestureRecognizer *gestureRecognizer;

@property (nonatomic, assign, getter=isBackgroundOnly) BOOL backgroundOnly; /// Whether to respond only to a click on the View, not a click on the SubView

@property (nonatomic, copy, nullable) DRGestureActionBlock action;

@property (nonatomic, weak) id actionTarget;
@property (nonatomic, assign) SEL actionSelector;

@end

NS_ASSUME_NONNULL_END
