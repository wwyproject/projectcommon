//
//  UIView+DRGesture.m
//  ProjectCommon
//
//  Created by DevWang on 2022/7/20.
//

#import "UIView+DRGesture.h"
#import "NSObject+DRExtraData.h"

@implementation UIView (DRGesture)

#pragma mark - Public Method
- (void)dr_performActionOnTap:(DRGestureActionBlock)action {
    [self dr_performActionOnTap:action backgroundOnly:NO];
}

- (void)dr_performActionOnTap:(DRGestureActionBlock)action backgroundOnly:(BOOL)isBackgroundOnly {
    self.dr_tapGestureElement.action = action;
    self.dr_tapGestureElement.backgroundOnly = isBackgroundOnly;
}

- (void)dr_performSelectorOnTapWithTarget:(id)target selector:(SEL)selector {
    [self dr_performSelectorOnTapWithTarget:target selector:selector backgroundOnly:NO];
}

- (void)dr_performSelectorOnTapWithTarget:(id)target selector:(SEL)selector backgroundOnly:(BOOL)isBackgroundOnly {
    self.dr_tapGestureElement.action = nil;
    self.dr_tapGestureElement.actionTarget = target;
    self.dr_tapGestureElement.actionSelector = selector;
    self.dr_tapGestureElement.backgroundOnly = isBackgroundOnly;
}

#pragma mark - Setter Getter
- (void)setDr_tapGestureElement:(DRGestureElement *)dr_tapGestureElement {
    [self dr_extraDataForSelector:@selector(dr_tapGestureElement)].data = dr_tapGestureElement;
}

- (DRGestureElement *)dr_tapGestureElement {
    DRGestureElement *element = [self dr_extraDataForSelector:@selector(dr_tapGestureElement)].data;
    if (!element) {
        element = [DRGestureElement new];
        element.target = self;
        self.dr_tapGestureElement = element;
    }
    
    return element;
}

@end


@implementation DRGestureElement

@synthesize gestureRecognizer = _gestureRecognizer;

#pragma mark - Protocol
#pragma mark <UIGestureRecognizerDelegate>
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if (self.isBackgroundOnly) {
        UIView *view = touch.view;
        if (view != self.target &&
            [view isDescendantOfView:self.target]) {
            return NO;
        }
    }
    
    return YES;
}

#pragma mark - Actions
- (void)gestureRecognizerDidRecognize:(UIGestureRecognizer *)gestureRecognizer {
    if (self.action) {
        self.action(self.target);
    }
    else if (self.actionTarget && self.actionSelector) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self.actionTarget performSelector:self.actionSelector withObject:self.target];
#pragma clang diagnostic pop
    }
}

#pragma mark - Setter Getter
- (void)setTarget:(UIView *)target {
    if (_target == target) {
        return;
    }
    
    [_target removeGestureRecognizer:self.gestureRecognizer];
    
    _target = target;
    
    [target addGestureRecognizer:self.gestureRecognizer];
    target.userInteractionEnabled = YES;
}

#pragma mark - Lazy Load
- (UIGestureRecognizer *)gestureRecognizer {
    if (!_gestureRecognizer) {
        UIGestureRecognizer *object = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureRecognizerDidRecognize:)];
        object.delegate = self;
        _gestureRecognizer = object;
    }
    
    return _gestureRecognizer;
}
@end
