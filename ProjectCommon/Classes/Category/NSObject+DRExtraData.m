//
//  NSObject+DRExtraData.m
//  ProjectCommon
//
//  Created by DevWang on 2022/7/20.
//

#import "NSObject+DRExtraData.h"
#import <objc/runtime.h>
@implementation NSObject (DRExtraData)

#pragma mark - Public Method
- (DRExtraData *)dr_extraDataForKey:(NSString *)key {
    DRExtraData * data = [[self dr_extraDataDictionary] objectForKey:key];
    if (!data) {
        data = [DRExtraData new];
        [[self dr_extraDataDictionary] setObject:data forKey:key];
    }
    
    return data;
}

- (DRExtraData *)dr_extraDataForSelector:(SEL)selector {
    return [self dr_extraDataForKey:NSStringFromSelector(selector)];
}

- (NSMutableDictionary *)dr_extraDataDictionary {
    NSMutableDictionary * dictionary = objc_getAssociatedObject(self, @selector(dr_extraDataDictionary));
    if (!dictionary) {
        dictionary = [NSMutableDictionary new];
        objc_setAssociatedObject(self, @selector(dr_extraDataDictionary), dictionary, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    
    return dictionary;
}

@end

@implementation DRExtraData

@end
