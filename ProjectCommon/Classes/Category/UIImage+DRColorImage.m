//
//  UIImage+DRColorImage.m
//  Pods-ProjectCommon_Example
//
//  Created by DevWang on 2022/7/20.
//

#import "UIImage+DRColorImage.h"

@implementation UIImage (DRColorImage)

+ (UIImage *)dr_imageWithColor:(UIColor *)color {
    return [self dr_imageWithColor:color size:CGSizeMake(1.0f, 1.0f)];
}

+ (UIImage *)dr_imageWithColor:(UIColor *)color size:(CGSize)size {
    if (!color || size.width <= 0 || size.height <= 0) {
        return nil;
    }
    CGRect rect = CGRectMake(0.0f, 0.0f, size.width, size.height);
    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, rect);
    
    UIImage *image = nil;
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
