//
//  UIApplication+DRUtils.m
//  ProjectCommon
//
//  Created by DevWang on 2022/7/21.
//

#import "UIApplication+DRUtils.h"

@implementation UIApplication (DRUtils)

+ (UIEdgeInsets)dr_safeAreaInsets {
    UIEdgeInsets insets = UIEdgeInsetsZero;
    if (@available(iOS 11.0, *)) {
        insets = [[UIApplication sharedApplication] delegate].window.safeAreaInsets;
    }
    return insets;
}

@end
