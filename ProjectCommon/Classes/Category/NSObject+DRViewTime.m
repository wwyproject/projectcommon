//
//  NSObject+DRViewTime.m
//  ProjectCommon
//
//  Created by DevWang on 2022/7/22.
//

#import "NSObject+DRViewTime.h"
#import <objc/runtime.h>

static void *kViewTimeAssociatedKey = &kViewTimeAssociatedKey;

@implementation NSObject (DRViewTime)

@dynamic beginTime;

- (void)setBeginTime:(NSInteger)beginTime {
    objc_setAssociatedObject(self, kViewTimeAssociatedKey, @(beginTime), OBJC_ASSOCIATION_COPY);
}

- (NSInteger)beginTime {
    NSNumber *time = objc_getAssociatedObject(self, kViewTimeAssociatedKey);
    NSInteger beginTime=  [time integerValue];
    return beginTime;
}

- (NSInteger)currentTime {
    NSInteger time = (NSInteger)CFAbsoluteTimeGetCurrent();
    return time;
}

- (void)beginRecordViewTime {
    self.beginTime = [self currentTime];
}

- (NSInteger)endRecordViewTime {
    NSInteger time = [self currentTime];
    NSInteger viewTime = time - self.beginTime;
    return viewTime;
}

@end
