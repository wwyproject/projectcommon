//
//  NSObject+DRExtraData.h
//  ProjectCommon
//
//  Created by DevWang on 2022/7/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class DRExtraData;
@interface NSObject (DRExtraData)

/// Shortcut set/get extra data from NSObject.
/// @param key string
- (DRExtraData *)dr_extraDataForKey:(NSString *)key;

/// Shortcut set/get extra data from NSObject.
/// @param selector selector
- (DRExtraData *)dr_extraDataForSelector:(SEL)selector;

@end

@interface DRExtraData : NSObject

@property (nonatomic, strong) id data;
@property (nonatomic, weak) id weakData;
@property (nonatomic, strong) NSMutableArray *array;

@end

NS_ASSUME_NONNULL_END
