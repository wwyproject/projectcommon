//
//  UILabel+DRUtils.h
//  Pods-ProjectCommon_Example
//
//  Created by DevWang on 2022/7/20.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

extern const CGFloat DRLabelLineHeightIdentity;

@interface UILabel (DRUtils)

@property (nullable, nonatomic, copy) NSDictionary<NSAttributedStringKey, id> *dr_textAttributes;

@property (nonatomic, assign) CGFloat dr_lineHeight;

@end

NS_ASSUME_NONNULL_END
