//
//  UIApplication+DRUtils.h
//  ProjectCommon
//
//  Created by DevWang on 2022/7/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIApplication (DRUtils)

+ (UIEdgeInsets)dr_safeAreaInsets;

@end

NS_ASSUME_NONNULL_END
