//
//  UIView+Extension.h
//  LotteryTicket
//
//  Created by Bad on 2018/11/14.
//  Copyright © 2018 Bad. All rights reserved.
//



NS_ASSUME_NONNULL_BEGIN

@interface UIView (Extension)

@property (readonly) CGPoint bottomLeft;
@property (readonly) CGPoint bottomRight;
@property (readonly) CGPoint topRight;

/**
 * Shortcut for frame.origin
 */
@property CGPoint origin;

/**
 * Shortcut for frame.size
 */
@property CGSize size;

/**
 * Shortcut for frame.size.height
 *
 * Sets frame.size.height = height
 */
@property CGFloat height;

/**
 * Shortcut for frame.size.width
 *
 * Sets frame.size.width = width
 */
@property CGFloat width;

/**
 * Shortcut for frame.origin.y
 *
 * Sets frame.origin.y = top
 */
@property CGFloat top;


@property CGFloat left;

/**
 * Shortcut for frame.origin.y + frame.size.height
 *
 * Sets frame.origin.y = bottom - frame.size.height
 */
@property CGFloat bottom;

/**
 * Shortcut for frame.origin.x + frame.size.width
 *
 * Sets frame.origin.x = right - frame.size.width
 */
@property CGFloat right;

/**
 * Shortcut for center.x
 *
 * Sets center.x = centerX
 */
@property (nonatomic) CGFloat centerX;

/**
 * Shortcut for center.y
 *
 * Sets center.y = centerY
 */
@property (nonatomic) CGFloat centerY;

//找到自己的vc
- (UIViewController *)nim_viewController;

@end

NS_ASSUME_NONNULL_END
