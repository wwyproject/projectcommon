//
//  UIView+Category.m
//  ProjectCommon
//
//  Created by DevWang on 2022/7/20.
//

#import "UIView+Category.h"

@implementation UIView (Category)

- (void)dr_roundCorners:(DRRectCorner)corners radius:(CGFloat)radius {
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds
                                                   byRoundingCorners:(UIRectCorner)corners
                                                         cornerRadii:CGSizeMake(radius, radius)];
    [maskPath closePath];
    shapeLayer.path = maskPath.CGPath;
    self.layer.mask = shapeLayer;
}

- (void)dr_removeAllSubviews {
    for (UIView *view in [self.subviews copy]) {
        [view removeFromSuperview];
    }
}

- (void)dr_addSubviews:(NSArray<UIView *> *)views {
    for (UIView *view in views) {
        [self addSubview:view];
    }
}

- (NSArray *)dr_addSubviewSpreadAutoLayout:(UIView *)view {
    if (!view || [self.subviews containsObject:view]) {
        return nil;
    }
    
    [self addSubview:view];
    view.translatesAutoresizingMaskIntoConstraints = NO;
    NSMutableArray *array = [NSMutableArray arrayWithArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[lysubview]-0-|"
                                                                                                   options:0
                                                                                                   metrics:nil
                                                                                                     views:@{@"lysubview" : view}]];
    
    [array addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[lysubview]-0-|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:@{@"lysubview" : view}]];
    
    NSArray *constraints = [NSArray arrayWithArray:array];
    [self addConstraints:constraints];
    return constraints;
}

- (BOOL)dr_isInView:(UIView *)view {
    if (!view) {
        return NO;
    }

    if (self == view) {
        return YES;
    }

    UIView *pView = self;
    do {
        pView = pView.superview;
        if (pView == view) {
            return YES;
        }
    } while (pView != nil);

    return NO;
}

- (CGRect)dr_convertRectToView:(UIView *)view {
    if (self == view) {
        return self.bounds;
    }
    CGRect rect = [self.superview convertRect:self.frame toView:view];
    return rect;
}


- (UIColor *)dr_colorWithPoint:(CGPoint)point {
    unsigned char pixel[4] = {0};

    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();

    CGContextRef context = CGBitmapContextCreate(pixel, 1, 1, 8, 4, colorSpace, kCGBitmapAlphaInfoMask & kCGImageAlphaPremultipliedLast);

    CGContextTranslateCTM(context, -point.x, -point.y);

    [self.layer renderInContext:context];

    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);

    UIColor *color = [UIColor colorWithRed:pixel[0] / 255.0f green:pixel[1] / 255.0f blue:pixel[2] / 255.0f alpha:pixel[3] / 255.0f];

    return color;
}

- (UIImage *)dr_snapshotImage {
    return [self dr_snapshotImageWithScale:0];
}

- (UIImage *)dr_snapshotImageWithScale:(CGFloat)scale {
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.opaque, scale);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *snap = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return snap;
}

- (UIImage *)dr_snapshotImageAfterScreenUpdates:(BOOL)afterUpdates {
    if (![self respondsToSelector:@selector(drawViewHierarchyInRect:afterScreenUpdates:)]) {
        return [self dr_snapshotImage];
    }
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.opaque, 0);
    [self drawViewHierarchyInRect:self.bounds afterScreenUpdates:afterUpdates];
    UIImage *snap = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return snap;
}

@end
