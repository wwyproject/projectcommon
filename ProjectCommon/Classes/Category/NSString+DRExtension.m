//
//  NSString+DRExtension.m
//  ProjectCommon
//
//  Created by DevWang on 2022/7/19.
//

#import "NSString+DRExtension.h"

@implementation NSString (DRExtension)

+ (CGSize)setTextSize:(NSString *)text Font:(float)font{
    CGSize statuseStrSize = [NSString  setTextSize:text FontSize:[UIFont systemFontOfSize:font]];
    return statuseStrSize;
}

/// 获取字符串的长度和宽度
/// @param text 字符串
/// @param fontSize 字符串尺寸
+ (CGSize)setTextSize:(NSString *)text FontSize:(UIFont *)fontSize {
    //计算文本文字size
    CGSize size = [text sizeWithAttributes:@{NSFontAttributeName:fontSize}];
    //获取宽高
    CGSize statuseStrSize = CGSizeMake(ceilf(size.width), ceilf(size.height));
    return statuseStrSize;
}


+ (CGSize)setTextWithString:(NSString *)str BoundingRectWithSize:(CGSize)size systemFontOfSize:(UIFont*)font{
    CGSize NewSize = [str boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil].size;
    return NewSize;
}

+ (CGSize)setTextWithString:(NSString *)str BoundingRectWithWidth:(CGFloat)width systemFont:(CGFloat)font {
    CGSize NewSize = [NSString setTextWithString:str BoundingRectWithWidth:width systemFontsize:[UIFont systemFontOfSize:font]];
    return NewSize;
}

+ (CGSize)setTextWithString:(NSString *)str BoundingRectWithWidth:(CGFloat)width systemFontsize:(UIFont*)font {
    CGSize NewSize = [str boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil].size;
    return NewSize;
}

+ (CGSize)setTextWithString:(NSString *)str BoundingRectWithHeight:(CGFloat)height systemFontsize:(UIFont*)font {
    CGSize NewSize = [str boundingRectWithSize:CGSizeMake(MAXFLOAT, height) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil].size;
    return NewSize;
}


+ (NSDate *)setNSStringToDate:(NSString *)string  dateFormat:(NSString *)dateformat{
    // 日期格式化类
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    // 设置日期格式 为了转换成功
    format.dateFormat = dateformat;
    // NSString * -> NSDate *
    NSDate *date = [format dateFromString:string];
    return date;
}

+ (NSString *)setDateToNSString:(NSDate *)date DateFormat:(NSString *)dateformat {
    // 日期格式化类
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    // 设置日期格式 为了转换成功
    format.dateFormat = dateformat;
    NSString *newString = [format stringFromDate:date];
    return newString;
}



/// 查询字符串中知否有目标字符
/// @param  queryString 查询字符串
/// @param string 目标字符
+ (BOOL)getWhetherTheQueryStringExists:(NSString *)queryString TargetString:(NSString *)string {
    if([queryString rangeOfString:string].location == NSNotFound) {
        return NO;
     } else {
        return YES;
    }
}




/// 将字符串 中 目标字符 替换
/// @param string 字符串
/// @param str 需要替换的字符
/// @param content 替换成的字符
+ (NSString *)ReplaceTargetString:(NSString *)string TargetString:(NSString *)str ReplaceContent:(NSString *)content {
    NSRange range = [string rangeOfString:str];
    NSString *str_r = [string stringByReplacingCharactersInRange:range withString:content];
    return str_r;
}

+ (NSString *)dr_jsonStringCompactFormatForNSArray:(NSArray *)arrJson {
    if (![arrJson isKindOfClass:[NSArray class]] || ![NSJSONSerialization isValidJSONObject:arrJson]) {
        return nil;

    }
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arrJson options:0 error:nil];
    NSString *strJson = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return strJson;
}


+ (NSString *)stringWithConfigurationColor:(NSString *)color {
    
    NSArray *colors = [color componentsSeparatedByString:@"#"];
    
    NSString *color_str = [NSString stringWithFormat:@"0x%@",colors[1]];
    
    return color_str;
}

@end
