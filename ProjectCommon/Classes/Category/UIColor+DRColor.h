//
//  UIColor+DRColor.h
//  DWProject
//
//  Created by DevWang on 2022/7/19.
//  Copyright © 2022 dreamworks. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

#ifndef DRColorRGB
#define DRColorRGB(r, g, b) \
[UIColor colorWithRed:(r) / 255.0f green:(g) / 255.0f blue:(b) / 255.0f alpha:1.0f]
#endif

#ifndef DRColorRGBA
#define DRColorRGBA(r, g, b, a) \
[UIColor colorWithRed:(r) / 255.0f green:(g) / 255.0f blue:(b) / 255.0f alpha:(a)]
#endif

#ifndef DRColorHex
#define DRColorHex(rgbValue) \
[UIColor dr_colorWithHexValue:rgbValue alpha:1.0f]
#endif

#ifndef DRColorHexA
#define DRColorHexA(rgbValue, a) \
[UIColor dr_colorWithHexValue:rgbValue alpha:a]
#endif

#ifndef DRColor
#define DRColor(light, dark) \
[UIColor dr_colorDynamicWithLight:light dark:dark]
#endif

#ifndef DRColorDynamic
#define DRColorDynamic(light, dark) \
[UIColor dr_colorDynamicWithLight:light dark:dark]
#endif

#ifndef DRColorDynamicA
#define DRColorDynamicA(light, dark, a) \
[UIColor dr_colorDynamicWithLight:light lightAlpha:1.0 dark:dark darkAlpha:a]
#endif

#ifndef DRColorDynamicAA
#define DRColorDynamicAA(light, a1, dark, a2) \
[UIColor dr_colorDynamicWithLight:light lightAlpha:a1 dark:dark darkAlpha:a2]
#endif


@interface UIColor (DRColor)

/// 随机颜色
/// @param alpha 透明度
+ (UIColor *)randomColor:(CGFloat)alpha NS_SWIFT_NAME(dr_randomColor(alpha:));

/// 设置颜色(默认透明度为1.0)
/// @param hexValue 16进制颜色
+ (UIColor *)dr_colorWithHexValue:(unsigned)hexValue NS_SWIFT_NAME(dr_color(_:));

/// 设置颜色
/// @param hexValue 16进制颜色
/// @param alpha 透明度
+ (UIColor *)dr_colorWithHexValue:(unsigned int)hexValue
                            alpha:(GLfloat)alpha NS_SWIFT_NAME(dr_color(_:alpha:));

/// 动态设置颜色
/// @param light 正常模式
/// @param dark 暗黑模式
+ (UIColor *)dr_colorDynamicWithLight:(unsigned)light
                                 dark:(unsigned)dark NS_SWIFT_NAME(dr_colorDynamic(_:_:));

/// 动态设置颜色
/// @param light 正常模式
/// @param light_alpha 透明度
/// @param dark 暗黑模式
/// @param dark_alpha 透明度
+ (UIColor *)dr_colorDynamicWithLight:(unsigned)light
                           lightAlpha:(GLfloat)light_alpha
                                 dark:(unsigned)dark
                            darkAlpha:(GLfloat)dark_alpha NS_SWIFT_NAME(dr_colorDynamic(light:light_a:dark:dark_a:));

@end

NS_ASSUME_NONNULL_END
