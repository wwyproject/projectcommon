//
//  UIColor+DRColor.m
//  DWProject
//
//  Created by DevWang on 2022/7/19.
//  Copyright © 2022 dreamworks. All rights reserved.
//

#import "UIColor+DRColor.h"

@implementation UIColor (DRColor)


+ (UIColor *)randomColor:(CGFloat)alpha {
    return [UIColor colorWithRed:((float)arc4random_uniform(256) / 255.0)
                           green:((float)arc4random_uniform(256) / 255.0)
                            blue:((float)arc4random_uniform(256) / 255.0) alpha:alpha];
}

+ (UIColor *)dr_colorWithHexValue:(unsigned)hexValue {
    return [UIColor dr_colorWithHexValue:hexValue alpha:1.0];
}

+ (UIColor *)dr_colorWithHexValue:(unsigned int)hexValue alpha:(GLfloat)alpha {
    unsigned red = 0.0f;
    unsigned green = 0.0f;
    unsigned blue = 0.0f;
    
    red = (hexValue & 0xFF0000) >> 16;
    green = (hexValue & 0xFF00) >> 8;
    blue = hexValue & 0xFF;
    
    return [UIColor colorWithRed:(float) red / 255.0
                           green:(float) green / 255.0
                            blue:(float) blue / 255.0 alpha:alpha];
}



+ (UIColor *)dr_colorDynamicWithLight:(unsigned)light
                                 dark:(unsigned)dark {
    return  [UIColor dr_colorDynamicWithLightColor:[UIColor dr_colorWithHexValue:light alpha:1.0] darkColor:[UIColor dr_colorWithHexValue:dark alpha:1.0]];
}

+ (UIColor *)dr_colorDynamicWithLight:(unsigned)light
                           lightAlpha:(GLfloat)light_alpha
                                 dark:(unsigned)dark
                            darkAlpha:(GLfloat)dark_alpha {
    return  [UIColor dr_colorDynamicWithLightColor:[UIColor dr_colorWithHexValue:light alpha:light_alpha] darkColor:[UIColor dr_colorWithHexValue:dark alpha:dark_alpha]];
}


+ (UIColor *)dr_colorDynamicWithLightColor:(UIColor *)lightColor
                                 darkColor:(UIColor *)darkColor {
    if (@available(iOS 13.0, *)) {
        return ([UIColor colorWithDynamicProvider:^UIColor * _Nonnull(UITraitCollection * _Nonnull traitCollection) {
            switch (traitCollection.userInterfaceStyle) {
                case UIUserInterfaceStyleDark:
                    return darkColor;
                default:
                    return lightColor;
            }
        }]);
    } else {
        return lightColor;
    }
}


@end
