//
//  LWViewController.m
//  ProjectCommon
//
//  Created by wang.wenyuan on 11/13/2021.
//  Copyright (c) 2021 wang.wenyuan. All rights reserved.
//

#import "LWViewController.h"
#import <ProjectCommon/DRCommonHeader.h>
#import <ProjectCommon/ProjectCommon-Swift.h>
#import <Masonry/Masonry.h>

@interface LWViewController ()

@end

@implementation LWViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = UIColor.whiteColor;
}

@end
