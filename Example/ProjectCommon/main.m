//
//  main.m
//  ProjectCommon
//
//  Created by wang.wenyuan on 11/13/2021.
//  Copyright (c) 2021 wang.wenyuan. All rights reserved.
//

@import UIKit;
#import "LWAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LWAppDelegate class]));
    }
}
